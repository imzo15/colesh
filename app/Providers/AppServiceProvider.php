<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Speciality;

use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('color', function($attribute, $value, $parameters, $validator) {
            $colors = array("Red", "Blue", "Green", "Yellow", "Gray", "Purple", "Pink", "Orange", "No Color");
            return in_array($value, $colors);
        });

        Validator::extend('speciality', function($attribute, $value, $parameters, $validator) {
            $specialities = Speciality::lists('name')->toArray();
            //$specialities = Speciality::select('name as nombre')->get();
            return in_array($value, $specialities);
        });



        Validator::extend('start_date', function($attribute, $value, $parameters, $validator) {
            $date = array("Today", "Pending");
            return in_array($value, $date);
        });

        Validator::extend('afteryesterday', function($attribute, $value, $parameters, $validator) {

            date_default_timezone_set('America/Chihuahua');
            $today= date('m-d-Y') ;
            $start_date = strtotime($value);

            $newformat = date('m-d-Y',$start_date);

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
