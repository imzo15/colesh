<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;


class FollowController extends Controller{

	
	public function getIndex()
	{
		$following = Auth::user()->following();
		$followRequestsSentPending = Auth::user()->followRequestsSentPending();
		$followRequestsReceivedPending = Auth::user()->followRequestsReceivedPending();
		$followers= Auth::user()->followedBy();
		//Include list of people you are following and people following you
		//Include list of people who sent you a follow request 
		return view('follow.following')
			->with('followRequestsSentPending', $followRequestsSentPending)
			->with('followRequestsReceivedPending', $followRequestsReceivedPending)
			->with('followers', $followers)
			->with('following', $following);
	}

	public function getAdd($username)
	{
		$user = User::where('username', $username)->first();
		//dd($user);
		if(!$user){
			return redirect()->route('home')->with('info', 'That user could not be found.');
		}
		//Checking if user is sending himself a friend request
		if(Auth::user()->_id === $user->_id){
			return redirect()->route('home');
		}

		//Checking if request was already sent and got here by manually typing url
		if(Auth::user()->hasFollowRequestReceivedPending($user) || Auth::user()->hasFollowRequestSentPendingToAccept($user) ){
			return redirect()->route('profile.index', ['username' => $username])->with('info', 'Follow request already pending.');
		}

		//Checking if are already following user and got here by manually typing url
		if(Auth::user()->isFollowing($user)){
			return redirect()->route('profile.index', ['username' => $username])->with('info', 'You are already following ' . $user->getFirstNameOrUsername() .'.');
		}

		Auth::user()->addFollow($user);
		return redirect()->route('profile.index', ['username' => $username])->with('info', 'Follow request sent.');

	}

	public function getAccept($username)
	{
		$user = User::where('username', $username)->first();
		//dd($user);
		if(!$user){
			return redirect()->route('home')->with('info', 'That user could not be found.');
		}

		//Checking if request was never received and got here by manually typing url
		if(!Auth::user()->hasFollowRequestReceivedPending($user) ){
			return redirect()->route('home');
		}

		Auth::user()->acceptFollowRequest($user);
		return redirect()->route('profile.index', ['username' => $username])->with('info', '	Friend request accepted.');



	}

	public function postDelete($username)
	{
		$user = User::where('username', $username)->first();
		//dd($user);
		if(!$user){
			return redirect()->route('home');
		}

		if(!Auth::user()->isFollowing($user)){
			return redirect()->back();
		}
		
		Auth::user()->deleteFollow($user);
		return redirect()->back()->with('info', 'You are no longer following ' . $username);

	}

}