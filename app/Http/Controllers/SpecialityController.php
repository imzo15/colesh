<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Speciality;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


class SpecialityController extends Controller{

	

	public function postSpeciality(Request $request)
	{

		$this->validate($request, [
            'speciality' => 'required|max:128',
        ]);

		Speciality::create([
	        'name'      => $request->speciality, 
	    ]);
        return Redirect::back()->with('info', 'Speciality created succesfully!.');
		

	}


}