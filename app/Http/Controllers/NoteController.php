<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Note;
use Illuminate\Http\Request;


class NoteController extends Controller{

	public function postNote(Request $request)
	{
		$this->validate($request, [
			'title' => 'required|max:128',
			'note' => 'required|max:1000',
		]);

		Note::create([
            'user_id' => Auth::user()->_id, 
            'title' => $request->title,
            'note' => $request->note,
        ]);
		return redirect()->route('home')->with('info', 'Note posted successtully');

	}

	public function postNoteReply(Request $request, $noteId)
	{
		$this->validate($request, [
			"reply-{$noteId}" => 'required|max:1000',
		],[
			'required' => 'The reply body is required.'
		]);
		$note = Note::notReply()->find($noteId);
		if(!$note){
			return redirect()->route('home')->with('info', 'Invalid action Reply to note.');
		}
		//Proceed only if you are following the user who posted the note and is not your own note
		if(!Auth::user()->isFollowing($note->user) && Auth::user()->_id !== $note->user->_id ){
			return redirect()->route('home')->with('info', 'Invalid action Reply to note not following.');
		}
		Note::create([
            'user_id' => Auth::user()->_id, 
            'parent_id' => $note->_id, 
            //'title' => $request->title,
            'note' => $request->input("reply-{$note->_id}"),
        ]);
		return redirect()->back();
	}
	
	public function getLike($noteId)		
	{
		$note = Note::find($noteId);
		if(!$note){
			return redirect()->route('home');
		}
		if(!Auth::user()->isFollowing($note->user)){
			return redirect()->route('home');
		}
		if(Auth::user()->hasLikedNote($note)){
			return redirect()->route('home');
		}
		$like = $note->likes()->create([]);
		Auth::user()->likes()->save($like);

		return redirect()->back();

	}
}