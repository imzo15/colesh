<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\Models\User;
use App\Models\Question;
use App\Models\Answer;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Request as ARequest;
use Response;

class MessageController extends Controller
{


    public function sendMessage(Request $request)
	{
    	if(ARequest::ajax()){
			$chatInput = Input::get('chatInput');
			$receiver_user_id = Input::get('receiver_user_id');
			$receiver = User::find($receiver_user_id);
			Message::create([
				'receiver_user_id' => $receiver_user_id,
				'receiver_username' => $receiver->username,
		        'sender_user_id' => Auth::user()->_id,
		        'sender_username' => Auth::user()->username,
		        'message' => $chatInput,
            	'read' => false,
			]);
			return 'success' . $chatInput . ' ' . $receiver_user_id;
		}else{
			return 'fail';
		}


    }

    public function getMessage(Request $request)
	{
    	if(ARequest::ajax()){
			$chatInput = Input::get('chatInput');
			$receiver_user_id = Input::get('receiver_user_id');
			$message = Message::where('receiver_user_id', Auth::user()->_id)->where('sender_user_id', $receiver_user_id)->where('read', false)->get();
			if($message->count()){
				//$message[0] = Message::find($message->_id);
				foreach ($message as $key => $value) {
					$value->read = true;
					$value->save();
				}
				return Response::json($message);
				
			}else{
				return 0;
			}

			return 'success' . $chatInput . ' ' . $receiver_user_id;
		}else{
			return 'fail';
		}


    }

    public function getAllMessages(Request $request)
	{
    	if(ARequest::ajax()){
			$receiver_user_id = Input::get('receiver_user_id');
			//return $receiver_user_id;
			$messages = Message::where(function($query) use ($receiver_user_id)
			{

			$query->where('receiver_user_id', '=', $receiver_user_id)
			->where('sender_user_id', '=', Auth::user()->_id);
			})
            ->orWhere(function($query) use ($receiver_user_id)
            {
                $query->where('receiver_user_id', '=', Auth::user()->_id)
                      ->where('sender_user_id', '=', $receiver_user_id);
            })
            ->get();

			return Response::json($messages);

		 }else{
		 	return 'fail';
		 }


    }

    /* Ajax function to rate the answer */
    public function rateAnswer()
    {
    	if(ARequest::ajax()){
			$id = Input::get('id');
			$score = Input::get('score');
			$user_id = Auth::user()->_id;
			$answer = Answer::find($id);
			if(!$answer){
				return 'no answer';	
			}
			$rating = Rating::where('user_id', Auth::user()->_id)->where('model_id', $id)->get();
			if($rating->isEmpty()){
				Rating::create([
					'user_id' => $user_id,
			        'model_id' => $id,
			        'type' => 'answer',
			        'rating' => $score,
				]);
				return 'success';
			}
			else
				return $rating;
		/*
		*/
		}else{
			return 'fail';
		}
    }





}
