<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Question;
use App\Models\Speciality;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image as ImageMaker;
use Request as ARequest;
use Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;

require 'simple_html_dom.php';

class QuestionController extends Controller
{


    public function postQuestion(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:128',
            'description' => 'required|max:1000',
            'speciality' => 'required|speciality',
            'fileToUpload' => 'image|max:2000|min:1',
        ]);

        $question = Question::create([
            'user_id' => Auth::user()->_id, 
            'title' => $request->title,
            'description' => $request->description,
        ]);
        if(Input::file('fileToUpload')){
            // UPLOAD_ERR_INI_SIZE
            // Value: 1; The uploaded file exceeds the upload_max_filesize directive in php.ini.
            if (Input::file('fileToUpload')->getError() == UPLOAD_ERR_INI_SIZE) {
                return Redirect::back()->with('fileToUploadErrorSize', 'The file to upload may not be greater than 2MB.');
            }
            if (Input::file('fileToUpload')->isValid()) {
                $image = Input::file('fileToUpload');
                $destinationPath = 'image_upload/question/'.Auth::user()->_id; // upload path
                $extension = Input::file('fileToUpload')->getClientOriginalExtension(); // getting image extension
                $fileName = substr(Auth::user()->_id, -4).mt_rand(111111,999999); // renameing image

                //Create folder if doesnt exists
                if (!file_exists($destinationPath)) 
                    mkdir($destinationPath, 0700, true);
                File::copy(Input::file('fileToUpload')->getRealPath(), $destinationPath.'/'. $fileName.'.'.$extension);
                // uploading file to given path
                $img = ImageMaker::make(Input::file('fileToUpload')->getRealPath())->resize(96, 96)->save($destinationPath . '/thumb_'.$fileName.'.'.$extension); 
                Image::create([
                    'model_id'      => $question->_id, 
                    'type'          => 'question',
                    'path'          => $destinationPath,
                    'name'          => $fileName,
                    'extension'     => $extension,
                ]);
            }
        }

        return redirect()->route('home')->with('info', 'Question posted successtully');

    }


    public function getQuestion($id)
    {

        $question = Question::find($id);
         // $answers = $question->answers()->get();
         // foreach ($answers as $answer) {
         //     //dd($answer);
         //     dd($answer->myRating());
         // }
        $chatusers = User::all();
         
        return view("question.index")
        ->with('chatusers', $chatusers)
        ->with('question', $question);

        //return view("question.index");
    }

    public function getQuestionResults(Request $request)
    {
        $specialities = Speciality::all();
        $questions = Question::whereRaw(array('$text' => [ '$search' => $request->search ]))
        ->project(array( 'score' => [ '$meta' => 'textScore' ] ))
        ->orderBy('score', ['$meta' => "textScore"])->get();
        $chatusers = User::all();
        return view("question.results")
        ->with('specialities', $specialities)
        ->with('chatusers', $chatusers)
        ->with('questions', $questions);
    }

}
