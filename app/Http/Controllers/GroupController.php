<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Group;
use App\Models\GroupUser;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Request as ARequest;
use Response;

class GroupController extends Controller{

	public function postGroup(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|min:3|max:128',
			'description' => 'required|max:1000',
		]);

		Group::create([
            'name' => $request->name,
            'description' => $request->description,
        ]);
		return redirect()->route('controlpanel')->with('info', 'Group created successtully');

	}

	public function addUser()
	{
	
		if(ARequest::ajax()){
			$user = User::where('username', Input::get('username'))->first();
			$group = Group::where('name', Input::get('groupname'))->first();

			if(!$user){
				return 'no user';	
			}
			if(!$group){
				return 'no group' . Input::get('groupname');	
			}
			GroupUser::create([
	            'group_id' => $group->_id,
	            'user_id' => $user->_id,
	        ]);
			return 'ok ';
		}else{
			return 'fail';
		}

	}

	public function removeUser()
	{
	
		if(ARequest::ajax()){
			$user = User::where('username', Input::get('username'))->first();
			$group = Group::where('name', Input::get('groupname'))->first();

			if(!$user){
				return 'no user';	
			}
			if(!$group){
				return 'no group' . Input::get('groupname');	
			}
			GroupUser::where('group_id', $group->_id)->where('user_id', $user->_id)->delete();
			return 'ok ';
		}else{
			return 'fail';
		}

	}

	public function getUser()
	{
		
		if(ARequest::ajax()){
			$group = Group::where('name', Input::get('groupname'))->first();
			if(!$group){
				return 'no group' . Input::get('groupname');	
			}
			return Response::json($group->users());
		}
	}
}