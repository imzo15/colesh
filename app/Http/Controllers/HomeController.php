<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Note;
use App\Models\User;
use App\Models\Question;

class HomeController extends Controller{

	public function index(){
		if(Auth::check()){
			//All notes 
			//$notes = Note::notReply()
			$notes = Note::notReply()->where(function($query){
				return $query->where('user_id', Auth::user()->_id)->orWhereIn('user_id', Auth::user()->following()->lists('_id'));
			})
			->orderBy('created_at', 'desc')
			->paginate(10);
			
			$questions = Question::paginate(10);
			$chatusers = User::all();
			return view('timeline.index')
 			->with('chatusers', $chatusers)
			->with('questions', $questions)
			->with('notes', $notes);
		}
		return view('home');
 	}
 	
}