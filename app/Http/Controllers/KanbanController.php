<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Tag;
use App\Models\Task;
use App\Models\Kanbanboard;
use App\Models\Kanbancolumn;
use App\Models\SuperiorSubordinate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Request as ARequest;
use Response;
use DateTime;

class KanbanController extends Controller
{
    public function getIndex()
    {
    	$user = Auth::user();
 		if (!$user) {
 			abort(404);
 		} else {
 			$kanbanboard = Kanbanboard::where('user_id', Auth::user()->_id)->first();
			if(!$kanbanboard){
				$this->createKanbanBoard($user->_id);
			}

 			$tasksPending = $user->kanbanboard()->first()->kanbancolumns()->where('name', 'Pending')->first()->tasks()->get();
        	$tasksTodo = $user->kanbanboard()->first()->kanbancolumns()->where('name', 'To-Do')->first()->tasks()->get();
        	$tasksToday = $user->kanbanboard()->first()->kanbancolumns()->where('name', 'Today')->first()->tasks()->get();
        	$tasksInprogress = $user->kanbanboard()->first()->kanbancolumns()->where('name', 'In-progress')->first()->tasks()->get();
        	$tasksDone = $user->kanbanboard()->first()->kanbancolumns()->where('name', 'Done')->first()->tasks()->get();
 			//dd($kanbanboard);
 			$tags = Tag::lists('name', 'name');
			$subordinates = SuperiorSubordinate::where('superior_id', $user->_id)->lists('subordinate_username', 'subordinate_id');
			$users = User::where('_id',$user->_id)->lists('_id', 'username');//Get my own user
			//For some reason I cant get the _id as key, so I got it as a value, and then flipped the key-value
			//dd(array_merge($subordinates->toArray(), array_flip( $users->toArray() ) ) );
 			$chatusers = User::all();
    		return view("kanban.index", compact('tags'))
 			->with('subordinates', array_merge($subordinates->toArray(), array_flip( $users->toArray() ) ) )//Merge subordinates and my own user to select as responsible for a task
 			->with('chatusers', $chatusers)
 			->with('tasksPending', $tasksPending)
 			->with('tasksTodo', $tasksTodo)
 			->with('tasksToday', $tasksToday)
 			->with('tasksInprogress', $tasksInprogress)
 			->with('tasksDone', $tasksDone)
 			->with('user', $user);
 		}
    }

    public function getReleased()
    {
    	$user = Auth::user();
 		if (!$user) {
 			abort(404);
 		} else {
 			$tasks = Task::where('user_id', Auth::user()->_id)->where('released', true)->get();
 			$chatusers = User::all();
    		return view("kanban.released")
 			->with('tasks', $tasks)
 			->with('chatusers', $chatusers)
 			->with('user', $user);
 		}	
    }


   private function createKanbanBoard($user_id)
   {
   		
		$kanbanboard = Kanbanboard::create([
            'user_id' 	=> $user_id, 
        ]);

        $columns = array("Pending", "To-Do", "Today", "In-progress", "Done");
        foreach ($columns as $key => $value) {
	        $column = Kanbancolumn::create([
		        'kanbanboard_id' 	=> $kanbanboard->_id,
		        'name' 				=> $value,
    		]);
        }
		
   }

    public function postTask(Request $request)
	{
		/* This validation involves two inputs so its done in a custom way. */
		$pendingflag = $todoflag = $todayflag = false;
		if($request->start_date == "" || $request->due_date == ""){
			$pendingflag = true;
		}else{
            /* Check if date is in a correct format */
            $start_date_check = date_create($request->start_date);
            $due_date_check = date_create($request->due_date);
			if (!$start_date_check || !$due_date_check) {
				$pendingflag = true;
			}
			date_default_timezone_set('America/Chihuahua');
            try{
            	$start_date = new DateTime($request->start_date);
            	$due_date = new DateTime($request->due_date);
            	$today= new DateTime(date('Y-m-d')) ;

            }catch(Exception $e){
            	$pendingflag = true;
            }
          	/* start_date has to be greater than or equal to today, and due_date has to be greater than  or equal to start_date */
            if( ($start_date->format('U') >= $today->format('U') ) && ( $due_date->format('U') >= $start_date->format('U') )){
            	//dd('correct');
            	if($start_date->format('U') == $today->format('U')){
            		if($start_date->format('U') == $due_date->format('U'))//start_date and due_date are today
            			$todayflag = true;
            		else//start_date is today, but due_date is in future
            			$todoflag = true;
            	}else{//start_date and due_date are in the future
            		$todoflag = true;
            	}
            }else{//due_date is less than start_date
            	$pendingflag = true;
            }
			
		}
		$validator = $this->validate($request, [
			'name' => 'required|min:3|max:128',
			'description' => 'required|max:1000',
			'tags' => 'required',
			'subordinates' => 'required',
			'color' => 'color',
		]);

		

		$colors = array(
				'#FF4D4D' => 'Red', 
			 	'#2332FF' => 'Blue', 
			 	'#1DD613' => 'Green', 
			 	'#F1F734' => 'Yellow', 
			 	'#858585' => 'Gray', 
			 	'#BA23FF' => 'Purple', 
			 	'#FF23F0' => 'Pink', 
			 	'#FFAE23' => 'Orange', 
			 	'#F5F5F5' => 'No Color'
			);
		$htmlColor = array_search($request->color, $colors);
		$subordinatesIds = $request->subordinates;
        $sync_id = substr(Auth::user()->_id, -14).mt_rand(111111,999999); // Unique id to identify tasks to be synchronized

		foreach ($subordinatesIds as $key => $value) {
			$kanbanboard = Kanbanboard::where('user_id', $value)->first();
			if(!$kanbanboard){
				$this->createKanbanBoard($value);
			}
			$kanbancolumn = null;
			$kanbancolumnindex = 0;

			if($todayflag){
				$kanbancolumn = $kanbanboard->kanbancolumns()->where('name', 'Today')->get();
				$kanbancolumnindex = 2;
			}
			elseif($todoflag){
				$kanbancolumn = $kanbanboard->kanbancolumns()->where('name', 'To-Do')->get();
				$kanbancolumnindex = 1;

			}else{
				$kanbancolumn = $kanbanboard->kanbancolumns()->where('name', 'Pending')->get();
				$kanbancolumnindex = 0;
				$pendingflag = true;
			}
			$usertask = User::find($value);
			$task = Task::create([
	            'user_id' 			=> $value, 
	            'user_username' 	=> $usertask->username, 
	            'user_creator_id' 	=> Auth::user()->_id, 
	            'sync_id'			=> $sync_id,
	            'name' 				=> $request->name,
	            'description' 		=> $request->description,
	            'color'		 		=> $htmlColor,
	            //'comment' 		=> $request->comment,
	            'pending'			=> $pendingflag,
	            'editable'			=> ($request->editabletask != null)?true:false,
	            'start_date' 		=> $request->start_date,
	            'due_date' 			=> $request->due_date,
	            'column_index' 		=> $kanbancolumnindex,
		        'column_id'			=> $kanbancolumn->first()->_id,
		        'kanban_id'			=> $kanbanboard->_id,
	        ]);
		}
    	$user = Auth::user();
		
        $tags = Tag::lists('name', 'name');
        $tasksPending = $user->kanbanboard()->first()->kanbancolumns()->where('name', 'Pending')->first()->tasks()->get();
        $tasksTodo = $user->kanbanboard()->first()->kanbancolumns()->where('name', 'To-Do')->first()->tasks()->get();
        $tasksToday = $user->kanbanboard()->first()->kanbancolumns()->where('name', 'Today')->first()->tasks()->get();
        $tasksInprogress = $user->kanbanboard()->first()->kanbancolumns()->where('name', 'In-progress')->first()->tasks()->get();
        $tasksDone = $user->kanbanboard()->first()->kanbancolumns()->where('name', 'Done')->first()->tasks()->get();
 		$subordinates = SuperiorSubordinate::where('superior_id', $user->_id)->lists('subordinate_username', 'subordinate_id');
		$users = User::where('_id',$user->_id)->lists('_id', 'username');//Get my own user
		$chatusers = User::all();
		return view("kanban.index")
			->with('subordinates', array_merge($subordinates->toArray(), array_flip( $users->toArray() ) ) )//Merge subordinates and my own user to select as responsible for a task
			->with('tags', $tags)
			->with('chatusers', $chatusers)
			->with('tasksPending', $tasksPending)
			->with('tasksTodo', $tasksTodo)
			->with('tasksToday', $tasksToday)
			->with('tasksInprogress', $tasksInprogress)
			->with('tasksDone', $tasksDone)
			->with('user', $user);

    }



	public function postTaskAjaxEdit()
	{
		
		
		if(ARequest::ajax()){
			$id = Input::get('id');
			$due_date = Input::get('due_date');
			$color = Input::get('color');
			$name = Input::get('name');
			$description = Input::get('description');
			$task = Task::find($id);
			if(!$task){
				return 'no task';		
			}
			if($task->editable != true){
				return 'not editable';
			}
			$task_sync_id = $task->sync_id;
			$tasks = Task::where('sync_id', $task_sync_id)->get();

			foreach ($tasks as $value) {
				$value->name = $name;
				$value->due_date = $due_date;
				$value->color = $color;
				$value->description = $description;
				$value->save();
			}
			return $task->color;
		/*
		*/
		}else{
			return 'fail';
		}
	}

	public function postTaskAjaxChangeColumn()
	{
		if(ARequest::ajax()){
			$user = Auth::user();
	 		if (!$user) {
	 			return 'error: not loggedin';
	 		}
			$task_id = Input::get('id');
			$column_name = Input::get('column_name');
        	$column = $user->kanbanboard()->first()->kanbancolumns()->where('name', $column_name)->first();
			$task = Task::find($task_id);

			if(!$task){
				return 'no task';	
			}
			$task->column_id = $column->_id;
			$task->pending = false;
			if($column_name == 'Pending'){
				$task->pending = true;
				$task->column_index = 0;				
			}
			elseif($column_name == 'To-Do'){
				$task->column_index = 1;				
			}
			elseif($column_name == 'Today'){
				$task->column_index = 2;				
			}
			elseif($column_name == 'In-progress'){
				$task->column_index = 3;				
			}
			elseif($column_name == 'Done'){
				$task->column_index = 4;				
			}
			$task->save();
				
		}else{
			return 'fail';
		}	
	}


	public function postTaskAjaxTodoToday()
 	{
 		if(ARequest::ajax()){
 			$user = Auth::user();
	 		if (!$user) {
	 			return 'error: not loggedin';
	 		}
        	$tasksTodo = $user->kanbanboard()->first()->kanbancolumns()->where('name', 'To-Do')->first()->tasks()->get();
        	$tasksToday = $user->kanbanboard()->first()->kanbancolumns()->where('name', 'Today')->first()->tasks()->get();
			//$obj_merged = (object) array_merge((array) $tasks1[0], (array) $tasks2[0]);

        	$array_tasks[] = $tasksTodo;
        	$array_tasks[] = $tasksToday;
			return $array_tasks;
			if(!$tasks){
				return 'no tasks';	
			}
			return Response::json($array_tasks);
		}


 	}

 	public function postTaskAjaxRelease()
 	{
 		if(ARequest::ajax()){
 			$user = Auth::user();
	 		if (!$user) {
	 			return 'error: not loggedin';
	 		}
			$task_id = Input::get('id');
			$task = Task::find($task_id);

        	if(!$task){
				return 'no task';	
			}
			$task->column_id = '';
			$task->pending = false;
			$task->column_index = null;				
			$task->released = true;
			$task->save();
			$task->released_at = $task->updated_at;
			$task->save();

			return Response::json($task);
		}else{
			return 'fail';
		}	
 	}

}
