<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Question;
use Illuminate\Http\Request;
use Request as ARequest;
use Response;
use DB;

class SearchController extends Controller{

	/**
	 * Returns list of users who match the search query.
	 * @param  Request $request [Search user query string]
	 * @return [redirect|App\Model\User]           [Redirects back home if the query is not present|Returns users]
	 */
	public function getResults(Request $request){
		$query = $request->input('query');
		
		if(!$query){
			return redirect()->route('home');
		}
		$users = User::where("CONCAT(first_name, ' ', last_name)", 'LIKE', "%{$query}%")->orWhere('username', 'LIKE', "%{$query}%")->get();
		//$users = User::where('username', 'like', "%{$query}%")->get();
		$chatusers = User::all();



		return view('search.results')
		->with('chatusers', $chatusers)
		->with('users', $users);
 	}

 	public function getSearch(Request $request)
 	{
 		if(ARequest::ajax()){
			//$question = DB::collection('questions')->raw()->find(array('$text'=>array('$search'=>$request->input('search'))))->getNext();
			$question = Question::whereRaw(array('$text'=>array('$search'=>$request->input('search'))))->get();
			//$question = Question::all();
			if(!$question){
				return 'no question' . $request->input('search');	
			}
			return Response::json($question);
		}


 	}
 	
}