<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Question;
use App\Models\Answer;
use App\Models\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Request as ARequest;
use Response;

class AnswerController extends Controller
{


    public function postAnswer(Request $request)
	{
		$this->validate($request, [
			'answer' => 'required|max:1000',
		]);

		Answer::create([
            'user_id' => Auth::user()->_id, 
            'question_id' => $request->question_id, 
            'answer' => $request->answer,
        ]);
		return redirect()->route('home')->with('info', 'Question posted successtully');



    }

    /* Ajax function to rate the answer */
    public function rateAnswer()
    {
    	if(ARequest::ajax()){
			$id = Input::get('id');
			$score = Input::get('score');
			$user_id = Auth::user()->_id;
			$answer = Answer::find($id);
			if(!$answer){
				return 'no answer';	
			}
			$rating = Rating::where('user_id', Auth::user()->_id)->where('model_id', $id)->get();
			if($rating->isEmpty()){
				Rating::create([
					'user_id' => $user_id,
			        'model_id' => $id,
			        'type' => 'answer',
			        'rating' => $score,
				]);
				return 'success';
			}
			else
				return $rating;
		/*
		*/
		}else{
			return 'fail';
		}
    }


/*    public function getQuestion($id)
    {
    	$question = Question::find($id);
		return view("question.index")
		->with('question', $question);

		//return view("question.index");
    }*/





}
