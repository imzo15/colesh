<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Note;
use App\Models\Speciality;
use App\Models\SpecialityUser;
use App\Models\LevelSpecialityUser;
use App\Models\SuperiorSubordinate;
use App\Models\Level;
use App\Models\Task;
use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Request as ARequest;
use Response;



class ControlPanelController extends Controller{

	public function getManageTasks(){
		if(ARequest::ajax()){
 			$tasksmanage = Task::where('user_creator_id', Auth::user()->_id)->where('user_id', '!=', Auth::user()->_id)->orderBy('user_id', 'desc')->get();
 			return $tasksmanage;
			
		}else{
			return 'fail';
		}		
	}




	public function getControlPanel()
	{
		$user = Auth::user();
 		if (!$user) {
 			abort(404);
 		} else {
 			$groups = Group::all();
 			$specialities = Speciality::all();
 			$chatusers = User::all();
 			$users = User::all();
 			$tasksmanage = Task::where('user_creator_id', Auth::user()->_id)->where('user_id', '!=', Auth::user()->_id)->orderBy('user_id', 'desc')->get();
 			return view('controlpanel.index')
 			->with('groups', $groups)
 			->with('specialities', $specialities)
 			->with('chatusers', $chatusers)
 			->with('tasksmanage', $tasksmanage)
 			->with('users', $users)
 			->with('user', $user);
 		}
	}

	public function addSpecialityUser()
	{
		if(ARequest::ajax()){
			$user = User::find(Input::get('user_id'));
			$speciality = Speciality::find(Input::get('speciality_id'));

			if(!$user){
				return 'no user';	
			}
			if(!$speciality){
				return 'no speciality';	
			}else{
				$specialityuser = LevelSpecialityUser::where('user_id', Input::get('user_id'))->where('speciality_id', Input::get('speciality_id'))->first();
				if(!$specialityuser){//If speciality is not yet in the document, add it and activate it
					LevelSpecialityUser::create([
			            'user_id' => $user->_id,
			            'speciality_id' => $speciality->_id,
			            'speciality_name' => $speciality->name,
			            'activated' => true,
			            'points' => 0,
			        ]);
					return 'ok ';
				}else{ //If its already in the document, deactivate it
					$specialityuser->activated = true;
					$specialityuser->save();
					return 'ok ';		
				}
			}
		}else{
			return 'fail';
		}
		
	}

	public function removeSpecialityUser()
	{
		if(ARequest::ajax()){
			$user = User::find(Input::get('user_id'));
			$speciality = LevelSpecialityUser::where('user_id', Input::get('user_id'))->where('speciality_id', Input::get('speciality_id'))->first();
			if(!$user){
				return 'no user';	
			}
			if(!$speciality){
				return 'no speciality';	
			}
			$speciality->activated = false;
			$speciality->save();
			return 'ok ';
		}else{
			return 'fail';
		}
		
	}


	public function getSpecialityUser()
	{

		if(ARequest::ajax()){
			$specialities = LevelSpecialityUser::where('user_id', Input::get('user_id'))->get();
			if(!$specialities){
				return 'no specialities';	
			}
			return Response::json($specialities);
		}else{
			return 'fail';
		}
		
	}

	public function getSpecialityAvailableUser()
	{

		if(ARequest::ajax()){
			$specialities = LevelSpecialityUser::where('user_id', Input::get('user_id'))->get(['speciality_id', 'speciality_name']);
			$allspecialities = Speciality::all();
			$resultArray = array_diff(array_column($allspecialities->toArray(), '_id'), array_column($specialities->toArray(), 'speciality_id'));
        	$specialitiesavailable = Speciality::find( $resultArray );

			//$specialitiesavailable = Speciality::whereNotIn('speciality_id', array_column($specialities->toArray(), 'speciality_id'))->get();
			if(!$specialitiesavailable){
				return 'no specialitiesavailable';	
			}
			return Response::json($specialitiesavailable);
		}else{
			return 'fail';
		}
		
	}

	public function postLevel(Request $request)
	{
	    $this->validate($request, [
            'level_name' => 'required|max:128',
            'points_required' => 'required|integer',
            'fileToUpload' => 'image|max:2000|min:1',
        ]);
	    $levels = Level::where('speciality_id', Input::get('speciality_id'))->get();
        $level = Level::create([
            'name' => $request->level_name,
            'level' => $levels->count() + 1,
            'points_required' => intval($request->points_required),
            'speciality_id' => $request->speciality_id,
    	]);
        $levels_specialities_users = LevelSpecialityUser::where('speciality_id', Input::get('speciality_id'))->where('points', '>', intval($request->points_required))->update(['level_id' => $level->_id]);

    	$user = Auth::user();
 		if (!$user) {
 			abort(404);
 		} else {
 			/* Change to return view(controlpanel.index) to get bug */
        	return redirect()->route('controlpanel')->with('info', 'Level posted successtully');
 		}
	}

	public function getLevels()
	{
		if(ARequest::ajax()){
			$levels = Level::where('speciality_id', Input::get('speciality_id'))->get();
			if(!$levels){
				return 'no levels';	
			}
			return Response::json($levels);
		}else{
			return 'fail';
		}	
	}


	/* Manage Superiors */
	// Get the subordinates available
	public function getSubordinatesAvailable()
	{
		if(ARequest::ajax()){
			$subordinates = SuperiorSubordinate::where('superior_id', Input::get('user_id'))->get(['subordinate_id']);
			$allusers = User::where('_id', '!=' ,Input::get('user_id'))->orWhereNull('_id')->get(['_id']);
			$resultArray = array_diff(array_column($allusers->toArray(), '_id'), array_column($subordinates->toArray(), 'subordinate_id'));
        	$subordinatesavailable = User::find( $resultArray );

			//$subordinatesavailable = Speciality::whereNotIn('speciality_id', array_column($specialities->toArray(), 'speciality_id'))->get();
			if(!$subordinatesavailable){
				return 'no subordinatesavailable';	
			}
			return Response::json($subordinatesavailable);
		}else{
			return 'fail';
		}
		
	}

	public function getSuperiorSubordinate()
	{
		if(ARequest::ajax()){
			$superior = User::find(Input::get('user_id'));

			if(!$superior){
				return 'no superior';	
			}else{
				$subordinates = SuperiorSubordinate::where('superior_id', Input::get('user_id'))->get();
				return $subordinates;
			}
		}else{
			return 'fail';
		}
		
	}

	public function addSuperiorSubordinate()
	{
		if(ARequest::ajax()){
			$superior = User::find(Input::get('superior_id'));
			$subordinate = User::find(Input::get('subordinate_id'));

			if(!$superior){
				return 'no superior';	
			}
			if(!$subordinate){
				return 'no subordinate';	
			}else{
				SuperiorSubordinate::create([
		            'superior_id' 			=> $superior->_id,
		            'superior_username' 	=> $superior->username,
		            'subordinate_id' 		=> $subordinate->_id,
		            'subordinate_username' 	=> $subordinate->username,
		        ]);
				return 'ok ';
			}
		}else{
			return 'fail';
		}
		
	}

	public function removeSuperiorSubordinate()
	{
		if(ARequest::ajax()){
			$superior = User::find(Input::get('superior_id'));
			$subordinate = User::find(Input::get('subordinate_id'));
			if(!$superior){
				return 'no superior';	
			}
			if(!$subordinate){
				return 'no subordinate';	
			}
			$superiorsubordinate = SuperiorSubordinate::where('superior_id', Input::get('superior_id'))->where('subordinate_id', Input::get('subordinate_id'))->first();
			$superiorsubordinate->delete();
			return 'ok ';
		}else{
			return 'fail';
		}
		
	}
	
}