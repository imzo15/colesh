<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;


class ProfileController extends Controller{

	/**
	 * Description: Gets the view of the user's profile. First it checks for the
	 * existenceof the user.
	 * @param  string $username | [User's username]
	 * @return view           | [Error 404 | User's profile View ]
	 */
	public function getProfile($username)
 	{
 		$user = User::where('username', $username)->first();
 		if (!$user) {
 			abort(404);
 		} else {
 			$notes = $user->notes()->notReply()->get();
 			$myQuestions = $user->myQuestions()->get();
 			$myAnswers = $user->myAnswers()->get();
 			$chatusers = User::all();
 			return view('profile.index')
 			->with('chatusers', $chatusers)
 			->with('user', $user)
 			->with('notes', $notes)
 			->with('myQuestions', $myQuestions)
 			->with('myAnswers', $myAnswers)
 			->with('authUserIsFollowing', Auth::user()->isFollowing($user));
 		}
 		
 	} 	
 	/**
 	 * Gets the view to edit user's profile. 
 	 * @return view          | [Users edit profile View]
 	 */
 	public function getEdit()
 	{
 		return view('profile.edit');
 	}

 	public function postEdit(Request $request)
 	{
 		$this->validate($request, [
 			'first_name' => 'alpha|max:50',
 			'last_name' => 'alpha|max:50',
 			'location' => 'max:20',
		]);

 		Auth::user()->update([
			'first_name' => $request->input('first_name'),
 			'last_name' => $request->input('last_name'),
 			'location' => $request->input('location'),
		]);

		return redirect()->route('profile.edit')->with('info', 'Your profile has been updated.');
 	}
}			