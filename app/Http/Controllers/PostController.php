<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Note;
use App\Models\Speciality;
use App\Models\SpecialityUser;
use App\Models\LevelSpecialityUser;
use App\Models\SuperiorSubordinate;
use App\Models\Level;
use App\Models\Task;
use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Request as ARequest;
use Response;



class PostController extends Controller{


	public function editPost()
	{
		$user = Auth::user();
 		if (!$user) {
 			abort(404);
 		} else {
 			$chatusers = User::all();
 			$users = User::all();
 			return view('post.index')
 			->with('chatusers', $chatusers)
 			->with('user', $user);
 		}
	}
	
	
}