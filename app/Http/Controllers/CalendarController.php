<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Note;
use Illuminate\Http\Request;


class CalendarController extends Controller{


	public function getCalendar()
	{
		$chatusers = User::all();
		return view('calendar.index')
 			->with('chatusers', $chatusers);
	}

	
}