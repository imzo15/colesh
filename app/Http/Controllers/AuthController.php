<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Testmongodb;


class AuthController extends Controller
{
	public function getSignUp(){
		return view('auth.signup');
	}

	public function postSignUp(Request $request){
		$this->validate($request, [
			'email' => 'required|unique:users|email|max:255',
			'username' => 'required|unique:users|alpha_dash|max:26',
			'password' => 'required|min:6',
		]);
		//Testmongodb::create(['name' => 'John']);
		User::create([
			'email' => $request->input('email'),
			'username' => $request->input('username'),
			'password' => bcrypt($request->input('password')),
		]);
		return redirect()->route('home')->with('info', 'Your account has been created and you can now sign in.');
	}

	public function getSignIn()
	{
		return view('auth.signin');
	}

	public function postSignIn(Request $request)
	{
		$this->validate($request, [
			'email' => 'required|email|max:255',
			'password' => 'required',
		]);

		if(!Auth::attempt($request->only(['email', 'password']), $request->has('remember') )){
			return redirect()->back()->with('info', 'Could not sign you in with those details.');
		}else{
			Auth::user()->update([
				'online' => true,
			]);
			return redirect()->route('home')->with('info', 'You are now signed in.');
		}
		
		
	}

	public function getSignOut()
	{
		Auth::user()->update([
				'online' => false,
			]);
		Auth::logout();
		return redirect()->route('home');
	}

}
