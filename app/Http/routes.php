<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Testmongodb;
use \App\Http\Controllers;

/* Test route*/
use App\Models\User;


/* End test route*/
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

	/* Test */
	Route::get('test', function(){
		$chatusers = User::all();
		return view('test')
 			->with('chatusers', $chatusers);
	});
	/* Home */
	Route::get('/',[
		'uses' => 'HomeController@index',
		'as' => 'home',
	]);
	/* Authentication */
	Route::get('/signup', [
		'uses' => 'AuthController@getSignUp',
		'as' => 'auth.signup',
		'middleware' => ['guest'],
	]);
	Route::post('/signup', [
		'uses' => 'AuthController@postSignUp',
		'middleware' => ['guest'],
	]);

	/* Sign In */
	Route::get('/signin', [
		'uses' => 'AuthController@getSignIn',
		'as' => 'auth.signin',
		'middleware' => ['guest'],
	]);
	Route::post('/signin', [
		'uses' => 'AuthController@postSignIn',
		'middleware' => ['guest'],
	]);

	/* Sign Out */
	Route::get('/signout', [
		'uses' => 'AuthController@getSignOut',
		'as' => 'auth.signout',
	]);
	
	/* Search */
	Route::get('/search', [
		'uses' => 'SearchController@getResults',
		'as' => 'search.results'
	]);

	/* User Profile */
	Route::get('/user/{username}', [
		'uses' => 'ProfileController@getProfile',
		'as' => 'profile.index',
		'middleware' => ['auth'],
	]);

	Route::get('/profile/edit', [
		'uses' => 'ProfileController@getEdit',
		'as' => 'profile.edit',
		'middleware' => ['auth'],
	]);

	Route::post('/profile/edit', [
		'uses' => 'ProfileController@postEdit',
		'as' => 'profile.edit',
		'middleware' => ['auth'],
	]);

	/* Friends */
	Route::get('/follow', [
		'uses' => 'FollowController@getIndex',
		'as' => 'follow.index',
		'middleware' => ['auth'],
	]);

	/* Control Panel */
	Route::get('/controlpanel', [
		'uses' => 'ControlPanelController@getControlPanel',
		'as' => 'controlpanel',
		'middleware' => ['auth'],
	]);	

	Route::get('/follow/add/{username}', [
		'uses' => 'FollowController@getAdd',
		'as' => 'follow.add',
		'middleware' => ['auth'],
	]);

	Route::get('/follow/accept/{username}', [
		'uses' => 'FollowController@getAccept',
		'as' => 'follow.accept',
		'middleware' => ['auth'],
	]);	

	Route::post('/follow/delete/{username}', [
		'uses' => 'FollowController@postDelete',
		'as' => 'follow.delete',
		'middleware' => ['auth'],
	]);	

	/* Notes */
	Route::post('/note', [
		'uses' => 'NoteController@postNote',
		'as' => 'note.post',
		'middleware' => ['auth'],
	]);

	Route::post('/note/{noteId}/reply', [
		'uses' => 'NoteController@postNoteReply',
		'as' => 'note.reply',
		'middleware' => ['auth'],
	]);	

	/* Likes Notes*/
	Route::get('/note/{noteId}/like', [
		'uses' => 'NoteController@getLike',
		'as' => 'note.like',
		'middleware' => ['auth'],
	]);


	/* Calendar */
	Route::get('/calendar', [
		'uses' => 'CalendarController@getCalendar',
		'as' => 'calendar.index',
		'middleware' => ['auth'],
	]);





	/* Group */
	Route::post('/group', [
		'uses' => 'GroupController@postGroup',
		'as' => 'group.post',
		'middleware' => ['auth'],
	]);

	/* Kanban */
	Route::get('/kanban', [
		'uses' => 'KanbanController@getIndex',
		'as' => 'kanban.index',
		'middleware' => ['auth'],
	]);


	Route::get('/kanban/released', [
		'uses' => 'KanbanController@getReleased',
		'as' => 'released.get',
		'middleware' => ['auth'],
	]);

	Route::post('/kanban', [
		'uses' => 'KanbanController@postTask',
		'as' => 'task.post',
		'middleware' => ['auth'],
	]);

	/* Speciality */
	Route::post('/speciality/add', [
		'uses' => 'SpecialityController@postSpeciality',
		'as' => 'speciality.post',
		'middleware' => ['auth'],
	]);

	/* Ajax request */
	Route::get('/kanbantask/changeColumn', [
		'uses' => 'KanbanController@postTaskAjaxChangeColumn',
		'as' => 'task.post.changeColumn',
		//Pending: allow to use middleware in post ajax
		'middleware' => ['auth'],
	]);

	/* Ajax request */
	Route::post('/kanbantask/realese', [
		'uses' => 'KanbanController@postTaskAjaxRealese',
		'as' => 'task.post.realese',
		//Pending: allow to use middleware in post ajax
		'middleware' => ['auth'],
	]);

	/* Ajax request */
	Route::get('/search/content', [
		'uses' => 'SearchController@getSearch',
		'as' => 'search.content',
		//Pending: allow to use middleware in post ajax
		'middleware' => ['auth'],
	]);

	/* Manage users tasks */
	Route::post('/managetasks/get', [
		'uses' => 'ControlPanelController@getManageTasks',
		'as' => 'managetasks.get',
		//Pending: allow to use middleware in post ajax
		'middleware' => ['auth'],
	]);	
	/* End Manage users tasks */


	/* Questions */
	Route::post('/question', [
		'uses' => 'QuestionController@postQuestion',
		'as' => 'question.post',
		'middleware' => ['auth'],
	]);

	/* Ajax Answer rate */
	Route::get('/answer/rate', [
		'uses' => 'AnswerController@rateAnswer',
		'as' => 'answer.rate',
		//Pending: allow to use middleware in post ajax
		'middleware' => ['auth'],
	]);

	/* Answers */
	Route::post('/answer', [
		'uses' => 'AnswerController@postAnswer',
		'as' => 'answer.post',
		'middleware' => ['auth'],
	]);

	/* Calendar */
	Route::get('/kanbantask/getTasksCalendar', [
		'uses' => 'KanbanController@postTaskAjaxTodoToday',
		'as' => 'task.post.getTasksCalendar',
		//Pending: allow to use middleware in post ajax
		//'middleware' => ['auth'],
	]);

	/* Question */
	Route::get('/question/results', [
		'uses' => 'QuestionController@getQuestionResults',
		'as' => 'question.results',
		'middleware' => ['auth'],
	]);

	Route::get('/question/{id}', [
		'uses' => 'QuestionController@getQuestion',
		'as' => 'question.index',
		'middleware' => ['auth'],
	]);

	/* Level */
	Route::post('/level/add', [
		'uses' => 'ControlPanelController@postLevel',
		'as' => 'level.post',
		'middleware' => ['auth'],
	]);

	/* Chat Message AJAX send*/
	Route::get('/message/send', [
		'uses' => 'MessageController@sendMessage',
		'as' => 'message.send',
		//Pending: allow to use middleware in post ajax
		'middleware' => ['auth'],
	]);

	/* Chat Message AJAX get all*/
	Route::get('/message/getAll', [
		'uses' => 'MessageController@getAllMessages',
		'as' => 'message.getall',
		//Pending: allow to use middleware in post ajax
		'middleware' => ['auth'],
	]);

	/* Chat Message AJAX get*/
	Route::get('/message/get', [
		'uses' => 'MessageController@getMessage',
		'as' => 'message.get',
		//Pending: allow to use middleware in post ajax
		'middleware' => ['auth'],
	]);


	/* Live Post */
	Route::get('/post/edit', [
		'uses' => 'PostController@editPost',
		'as' => 'post.edit',
		//Pending: allow to use middleware in post ajax
		'middleware' => ['auth'],
	]);


});
	/* ******** AJAX CONTROL PANEL ********* */
	Route::post('/groupuser/add', [
		'uses' => 'GroupController@addUser',
		'as' => 'groupuser.add',
		//Pending: allow to use middleware in post ajax
		//'middleware' => ['auth'],
	]);
	Route::post('/groupuser/remove', [
		'uses' => 'GroupController@removeUser',
		'as' => 'groupuser.remove',
		//Pending: allow to use middleware in post ajax
		//'middleware' => ['auth'],
	]);	

	Route::post('/groupuser/get', [
		'uses' => 'GroupController@getUser',
		'as' => 'groupuser.get',
		//Pending: allow to use middleware in post ajax
		//'middleware' => ['auth'],
	]);
	Route::post('/specialityuser/get', [
		'uses' => 'ControlPanelController@getSpecialityUser',
		'as' => 'specialityuser.get',
		//Pending: allow to use middleware in post ajax
		//'middleware' => ['auth'],
	]);
	Route::post('/specialityavailableuser/get', [
		'uses' => 'ControlPanelController@getSpecialityAvailableUser',
		'as' => 'specialityavailableuser.get',
		//Pending: allow to use middleware in post ajax
		//'middleware' => ['auth'],
	]);

	Route::post('/specialityuser/add', [
		'uses' => 'ControlPanelController@addSpecialityUser',
		'as' => 'specialityuser.add',
		//Pending: allow to use middleware in post ajax
		//'middleware' => ['auth'],
	]);
	Route::post('/specialityuser/remove', [
		'uses' => 'ControlPanelController@removeSpecialityUser',
		'as' => 'specialityuser.remove',
		//Pending: allow to use middleware in post ajax
		//'middleware' => ['auth'],
	]);

	Route::post('/level/get', [
		'uses' => 'ControlPanelController@getLevels',
		'as' => 'level.get',
		//Pending: allow to use middleware in post ajax
		//'middleware' => ['auth'],
	]);
	/* Manage superiors */
	Route::post('/subordinatesavailable/get', [
		'uses' => 'ControlPanelController@getSubordinatesAvailable',
		'as' => 'subordinatesavailable.get',
		//Pending: allow to use middleware in post ajax
		//'middleware' => ['auth'],
	]);
	Route::post('/superiorsubordinate/get', [
		'uses' => 'ControlPanelController@getSuperiorSubordinate',
		'as' => 'superiorsubordinate.get',
		//Pending: allow to use middleware in post ajax
		//'middleware' => ['auth'],
	]);
	Route::post('/superiorsubordinate/add', [
		'uses' => 'ControlPanelController@addSuperiorSubordinate',
		'as' => 'superiorsubordinate.add',
		//Pending: allow to use middleware in post ajax
		//'middleware' => ['auth'],
	]);
	Route::post('/superiorsubordinate/remove', [
		'uses' => 'ControlPanelController@removeSuperiorSubordinate',
		'as' => 'superiorsubordinate.remove',
		//Pending: allow to use middleware in post ajax
		//'middleware' => ['auth'],
	]);
	/* End Manage superiors */


	
	/* ******** END AJAX CONTROL PANEL ********* */

	Route::post('/kanbantask/edit', [
		'uses' => 'KanbanController@postTaskAjaxEdit',
		'as' => 'task.post.edit',
		//Pending: allow to use middleware in post ajax
		//'middleware' => ['auth'],
	]);




	

	


	


	


	
