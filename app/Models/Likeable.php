<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;


class Likeable extends Eloquent
{ 
    protected $collection = 'likeable';

    protected $fillable = [
        'user_id',
        'likeable_id',
        'likeable_type', 
    ];

    public function likeable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\Models\user', 'user_id');
    }
}
