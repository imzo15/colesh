<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


use Illuminate\Database\Eloquent\Model;


class Follower extends Eloquent
{


    protected $collection = 'followers';

    protected $fillable = [
        'follower_id',
        'followed_id', 
        'accepted',
    ];


}
