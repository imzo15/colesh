<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Illuminate\Database\Eloquent\Model;


class GroupUser extends Eloquent
{ 
    protected $collection = 'groups_users';

    protected $fillable = [
        'user_id',
        'group_id',
    ];



    public function users()
    {
    	return $this->hasMany('App\Models\User', '_id');
    }


}
