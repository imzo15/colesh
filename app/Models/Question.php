<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Illuminate\Database\Eloquent\Model;


class Question extends Eloquent
{


    protected $collection = 'questions';

    protected $fillable = [
        'user_id',
        'speciality_id',
        'title',
        'description',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', '_id');
    }

    public function speciality()
    {
        return $this->belongsTo('App\Models\Speciality', 'speciality_id', '_id');
    }

    public function answers()
    {
        return $this->hasMany('App\Models\Answer', 'question_id', '_id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image', 'model_id', '_id');
    }

}
