<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Illuminate\Database\Eloquent\Model;


class Group extends Eloquent
{ 
    protected $collection = 'groups';

    protected $fillable = [
        'name',
        'description', 
    ];



    public function users()
    {
    	$usersId = GroupUser::where('group_id', $this->_id)->get(['user_id']);
        //dd($usersId);
        return User::find( array_column($usersId->toArray(), 'user_id') );
    }


}
