<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;


class Note extends Eloquent
{ 
    protected $collection = 'notes';

    protected $fillable = [
        'user_id',
        'parent_id',
        'title', 
        'note',
        'likes',
    ];

    public function user()
    {
    	return $this->belongsTo('App\Models\User', 'user_id', '_id');
    }

    public function scopeNotReply($query)
    {
    	return $query->whereNull('parent_id');
    }

    public function replies()
    {
    	return $this->hasMany('App\Models\Note', '_id', 'parent_id');
    }

    public function likes()
    {
    	return $this->morphMany('App\Models\Likeable', 'likeable');
    }


}
