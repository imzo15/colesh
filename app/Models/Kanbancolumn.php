<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;


class Kanbancolumn extends Eloquent
{ 
    protected $collection = 'kanbancolumns';

    protected $fillable = [
        'kanbanboard_id',
        'name',
        'comments',
        'color',
        'max',
    ];

    public function kanbanboard()
    {
        return $this->belongsTo('App\Models\KanbanBoard', '_id', 'kanbanboard_id');
    }

    public function tasks()
    {
        return $this->hasMany('App\Models\Task', 'column_id', '_id');
    }



}
