<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Illuminate\Database\Eloquent\Model;


class SpecialityUser extends Eloquent
{


    protected $collection = 'specialities_users';

    protected $fillable = [
        'user_id',
        'speciality_id',
        'speciality_name',
        'activated',
    ];


}
