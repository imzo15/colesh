<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Illuminate\Database\Eloquent\Model;


class Image extends Eloquent
{


    protected $collection = 'images';

    protected $fillable = [
        'model_id',
        'type',
        'title',
        'path',
        'name',
        'extension',
    ];

    public function user()
    {
    	return $this->belongsTo('App\Models\User', 'user_id', '_id');
    }

    public function answers()
    {
        return $this->hasMany('App\Models\Answer', 'question_id', '_id');
    }

}
