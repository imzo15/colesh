<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Illuminate\Database\Eloquent\Model;


class Level extends Eloquent
{


    protected $collection = 'levels';

    protected $fillable = [
        'name',
        'speciality_id',
        'level',
        'image_path',
        'points_required',
    ];


}
