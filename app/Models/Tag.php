<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;


class Tag extends Eloquent
{ 
    protected $collection = 'tags';

    protected $fillable = [
        'name',
    ];



    /**
     * Get the tasks associated with the given task
     * @return Task
     */
    public function tasks()
    {
        return $this->belongsToMany('App\Models\Task', 'tag_taks', 'task_id', 'tag_id');
    }


}
