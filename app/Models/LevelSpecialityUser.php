<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Illuminate\Database\Eloquent\Model;


class LevelSpecialityUser extends Eloquent
{


    protected $collection = 'levels_specialities_users';

    protected $fillable = [
        'user_id',
        'speciality_id',
        'speciality_name',
        'activated',
        'level_id',
        'points',
    ];


}
