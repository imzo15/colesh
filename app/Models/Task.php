<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;


class Task extends Eloquent
{ 
    protected $collection = 'tasks';

    protected $fillable = [
        'user_id',
        'user_username',
        'user_creator_id',
        'sync_id',
        'name',
        'description', 
        'color',
        'editable',
        'comment',
        'start_date',
        'due_date',
        'done_date',
        'pending',
        'realesed',
        'realesed_at',
        'column_index',//0- Pending 1- To-Do 2- Today 3- In-Progress 4- Done
        'column_id',
        'kanban_id',
    ];

    public function user()
    {
    	return $this->belongsTo('App\Models\User', 'user_id', '_id');
    }

    /**
     * Get the tags associated with the given task
     * @return Tag
     */
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }


}
