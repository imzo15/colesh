<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Illuminate\Database\Eloquent\Model;


class Message extends Eloquent
{


    protected $collection = 'messages';

    protected $fillable = [
        'receiver_user_id',
        'receiver_username',
        'sender_user_id',
        'sender_username',
        'message',
        'read',
    ];

    protected $appends = ['sent',];

    public function getSentAttribute()
    {
        return $this->attributes['created_at']->toDateTime()->format('g:ia d/F/y');
    }

    public function receiver()
    {
        return $this->belongsTo('App\Models\User', 'receiver_user_id', '_id');
    }

    public function sender()
    {
        return $this->belongsTo('App\Models\User', 'sender_user_id', '_id');
    }


}
