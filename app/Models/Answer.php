<?php

namespace App\Models;

use Auth;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Illuminate\Database\Eloquent\Model;
use App\Models\Rating;


class Answer extends Eloquent
{


    protected $collection = 'answers';

    protected $fillable = [
        'user_id',
        'question_id',
        'answer',
        'likes',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', '_id');
    }

    public function question()
    {
        return $this->belongsTo('App\Models\Question', 'question_id', '_id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image', 'model_id', '_id');
    }

    public function myRating()
    {
        //return $this->belongsTo('App\Models\Rating', '_id', 'model_id');
        return Rating::where('user_id', Auth::user()->_id)->where('model_id', $this->_id)->get();
    }

}
