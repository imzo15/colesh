<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Illuminate\Database\Eloquent\Model;


class SuperiorSubordinate extends Eloquent
{


    protected $collection = 'superiors_subordinates';

    protected $fillable = [
        'superior_id',
        'superior_username',
        'subordinate_id',
        'subordinate_username',
    ];

    public function superior()
    {
        return $this->belongsTo('App\Models\User', 'superior_id', '_id');
    }

    public function subordinate()
    {
        return $this->belongsTo('App\Models\User', 'subordinate_id', '_id');
    }


}
