<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;


class TagTask extends Eloquent
{ 
    protected $collection = 'tag_task';

    protected $fillable = [
        'task_id',
        'tag_id',
    ];


}
