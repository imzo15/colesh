<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Illuminate\Database\Eloquent\Model;


class Speciality extends Eloquent
{


    protected $collection = 'specialities';

    protected $fillable = [
        'name',
        'description',
    ];

}
