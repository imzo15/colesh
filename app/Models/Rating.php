<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Illuminate\Database\Eloquent\Model;


class Rating extends Eloquent
{


    protected $collection = 'ratings';

    protected $fillable = [
        'user_id',
        'model_id',
        'type',
        'rating',
    ];

    public function user()
    {
    	return $this->belongsTo('App\Models\User', 'user_id', '_id');
    }

}
