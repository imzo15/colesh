<?php

namespace App\Models;


use Auth;
use App\Models\Note;
use App\Models\Question;
use App\Models\Answer;
use Illuminate\Auth\Authenticatable;
//use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;


class User extends Eloquent implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $collection = 'users';

    protected $fillable = [
        'username',
        'email', 
        'password',
        'first_name',
        'last_name',
        'online',
        'location',
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getName()
    {
        if ($this->first_name && $this->last_name) {
            return "{$this->first_name} {$this->last_name}";
        } 
        if($this->first_name){
            return $this->first_name;
        }
        return null;
        
    }

    public function getNameOrUsername()
    {
        return $this->getName() ?: $this->username;
    }

    public function getFirstNameOrUsername()
    {
        return $this->first_name ?: $this->username;
    }

    public function getAvatarUrl()
    {
        return "https://www.gravatar.com/avatar/{{ md5($this->email) }}?d=mm&s=40";
    }

    /**
     * Returns users who I follow, have accepted request or not.
     * IS NOT USED
     * @return \App\Models\User [Users who I sent follow request]
     */
    public function follow()
    {
        return $this->belongsToMany('App\Models\User', 'followers', 'follower_id', 'followed_id');
    }

    /**
     * Returns users who are following me.
     * @return \App\Models\User [Users who are following me]
     */
    public function followedBy()
    {
        $followed = Follower::where('followed_id',Auth::user()->_id)->where('accepted', true)->get(['follower_id']);
        //dd($followed->toArray());
        return User::find( array_column($followed->toArray(), 'follower_id') );

    }

    /**
     * Returns users who I am currently following. (Only the ones that have accepted my request)
     * @return App\Models\User [Users who I am following]
     */
    public function following()
    {
        $followed = Follower::where('follower_id',Auth::user()->_id)->where('accepted', true)->get(['followed_id']);
        //dd($followed->toArray());
        return User::find( array_column($followed->toArray(), 'followed_id') );

    }


   
   /**
    * Returns requests that are for you
    * @return App\Models\User [Users who sent me a follow request and I have not accepted]
    */
   public function followRequestsSentPending()
   {
        $followRequestPending = Follower::where('follower_id',Auth::user()->_id)->where('accepted', false)->get(['followed_id']);
        return User::find( array_column($followRequestPending->toArray(), 'followed_id') );
   }

   public function hasFollowRequestSentPendingToAccept(User $user)
   {
        return (bool) Follower::where('follower_id',Auth::user()->_id)->where('followed_id',$user->_id)->where('accepted', false)->count();
   }

    /**
    * Returns requests for you that are pending to accept
    * @return App\Models\User [Users who sent me a follow request and I have not accepted]
    */
   public function followRequestsReceivedPending()
   {
        $followRequestPending = Follower::where('followed_id',Auth::user()->_id)->where('accepted', false)->get(['follower_id']);
        return User::find( array_column($followRequestPending->toArray(), 'follower_id') );
   }

   public function hasFollowRequestReceivedPending(User $user)
   {
        //dd( (bool) Follower::where('followed_id',Auth::user()->_id)->where('follower_id',$user->_id)->where('accepted', false)->count() );
        return (bool) Follower::where('followed_id',Auth::user()->_id)->where('follower_id',$user->_id)->where('accepted', false)->count();
   }
  

    public function addFollow(User $user)
    {
        //dd( $user->_id . ' ' . Auth::user()->_id );
        Follower::create([
            'followed_id' => $user->_id,
            'follower_id' => Auth::user()->_id, 
            'accepted' => false,
        ]);
    }

    public function deleteFollow(User $user)
    {
        Follower::where('followed_id', $user->_id)->where('follower_id', Auth::user()->_id)->delete();
    } 

    public function acceptFollowRequest(User $user)
    {
      
        $follower = Follower::where('follower_id', $user->_id)->where('followed_id',Auth::user()->_id)->update(['accepted' => true]);

    }

    public function isFollowing(User $user)
    {
        return (bool) Follower::where('follower_id',Auth::user()->_id)->where('followed_id',$user->_id)->where('accepted', true)->count();
    }

    public function isFollowingMe(User $user)
    {
        return (bool) Follower::where('followed_id',Auth::user()->_id)->where('follower_id',$user->_id)->where('accepted', true)->count();
    }

    public function isMyProfile($url)
    {
        return (bool) strpos($url,Auth::user()->username);  
    }


    /* NOTES */

    public function notes()
    {
        return $this->hasMany('App\Models\Note', 'user_id');
        //return Note::where('user_id', Auth::user()->_id);
    }

    /*  LIKES  */
    public function hasLikedNote(Note $note)
    {
        return (bool) $note->likes
        ->where('user_id', $this->_id)
        ->count();
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Likeable', 'user_id');
    }

    /*  GROUP   */
    public function groups()
    {
        return $this->belongsTo('App\Models\Group', 'user_id', '_id');
    }

     /*  KANBAN BOARD   */
    public function kanbanboard()
    {
        return $this->belongsTo('App\Models\Kanbanboard', '_id', 'user_id');
    }


    /* QUESTIONS */
    public function myQuestions()
    {
        return Question::where('user_id', Auth::user()->_id);
    }


    /* ANSWERS */
    public function myAnswers()
    {
        return Answer::where('user_id', Auth::user()->_id);
    }

}
