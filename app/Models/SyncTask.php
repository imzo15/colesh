<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;


class SyncTask extends Eloquent
{ 
    protected $collection = 'synctasks';

    protected $fillable = [
        'sync_id',
        'task_id',
        'user_task_id',
        'last_edit_user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_task_id', '_id');
    }

    public function lastEditUser()
    {
        return $this->belongsTo('App\Models\User', 'last_edit_user_id', '_id');
    }



}
