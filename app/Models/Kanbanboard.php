<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;


class Kanbanboard extends Eloquent
{ 
    protected $collection = 'kanbanboards';

    protected $fillable = [
        'user_id',
        'name',
        'comments',
    ];


    public function users()
    {
    	return $this->hasMany('App\Models\User', '_id');
    }

    public function kanbancolumns()
    {
        return $this->hasMany('App\Models\Kanbancolumn', 'kanbanboard_id', '_id');
    }

}
