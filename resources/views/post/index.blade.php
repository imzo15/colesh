@extends('templates.default')
@section('title')
Live Post
@stop
@section('content')
<br>

		<div class="row">

			<div class="col-md-12">
				<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }} ">
		  			<input type="text" class="form-control" placeholder="Title">
					@if($errors->has('title'))
						<span class="help-block">{{ $errors->first('title') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-12">
				<textarea name="" id="" rows="10" style="width:100%;"></textarea>
				<img src="https://upload.wikimedia.org/wikipedia/commons/3/30/OmahaNE_Aerial.jpg" width="100%" alt="">
				<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }} ">
		  			<input type="text" class="form-control" placeholder="Title">
					@if($errors->has('title'))
						<span class="help-block">{{ $errors->first('title') }}</span>
					@endif
				</div>
			</div>
			
			<br><br><br><br><br>
			<div class="col-md-1">
				<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    	<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add <span class="caret"></span>
			  	</button>
			  	<ul class="dropdown-menu dropdown-post-section">
		    		<li class="dropdown-post-section-item"><a href="#">Subtitle</a></li>
			    	<li class="dropdown-post-section-item"><a href="#">Image</a></li>
			    	<li class="dropdown-post-section-item"><a href="#">Textarea</a></li>
			  	</ul>
			</div>
			<div class="col-md-10">
			</div>
			<div class="col-md-1">
				<input type="submit" class="form-control btn btn-primary pull-right" value="Save" />
			</div>

		</div>

@stop