@extends('templates.default')
@section('title')
Hello
@stop
@section('content')
<br>

	

	<div class="row">
		{{-- <form role="form" action=" {{ route('question.get.results') }} " method="get"> --}}
		{{ Form::open(array('url' => 'question/results', 'method' => 'GET')) }}
			<div class="col-md-10">
				<div class="form-group {{ $errors->has('search') ? ' has-error' : '' }} ">
					<input type="text" name="search" class="form-control" id="search" placeholder="Search content" autofocus>
					@if($errors->has('search'))
						<span class="help-block">{{ $errors->first('search') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-2">
					<input type="submit" class="form-control btn btn-default" value="Ask" />
			</div>
		{{ Form::close() }}
		{{-- </form> --}}
	</div>
	<hr>
	<hr>
	<div class="row">
		<div class="col-lg-6">
			<label for="recent-question" class="control-label">Recent questions</label>
			@foreach($questions as $question)
				<a href="{{ route('question.index', ['id' => $question->_id]) }}" class="question-panel">
					<div class="panel panel-info question-panel-info"> 
						<div class="panel-heading"> <h3 class="panel-title">{{ $question->title }}</h3> </div> 
						<div class="panel-body">{{ $question->description }}</div> 
					</div>
				</a>
			@endforeach


		</div>
		<div class="col-lg-6">
			<form role="form" action=" {{ route('note.post') }} " method="post">
		    	<div class="form-group {{ $errors->has('title-note') ? ' has-error' : '' }} ">
					<label for="title-note" class="control-label">Title note</label>
					<input type="text" name="title" class="form-control" id="title-note" placeholder="Title note" autofocus>
					@if($errors->has('title-note'))
						<span class="help-block">{{ $errors->first('title-note') }}</span>
					@endif
				</div>

				<div class="form-group {{ $errors->has('note') ? ' has-error' : '' }} ">
					<textarea name="note" id="note" cols="30" rows="2" placeholder="Your note here {{ Auth::user()->getFirstNameOrUsername() }}..." class="form-control"></textarea>
					@if($errors->has('note'))
						<span class="help-block">{{ $errors->first('note') }}</span>
					@endif
				</div>
				<button type="submit" class="btn btn-default">Post Note</button>
				{!! Form::token() !!}

			</form>
			<hr>
			{{-- Notes and replies --}}
			@if(!$notes->count())
				<p>Theere is nothing in your timeline, add a note.</p>
			@else
				@foreach($notes as $note)
					<div class="media">
						<a href="{{ route('profile.index', ['username' => $note->user->username]) }}" class="pull-left">
							<img src="{{ $note->user->getAvatarUrl() }}" alt=" {{ $note->user->getNameOrUsername() }} " class="media-object">
						</a>
						<div class="media-body">
							<h4 class="media-heading"><a href="{{ route('profile.index', ['username' => $note->user->username]) }}">{{ $note->user->getNameOrUsername() }}</a></h4>
							<p>{{ $note->note }}</p>
							<ul class="list-inline">
								<li>{{ $note->created_at->diffForHumans() }}</li>
								@if($note->user->_id !== Auth::user()->_id )
									<li><a href="{{ route('note.like', ['noteId' => $note->_id]) }}">Like</a></li>
								@endif
								<li> {{ $note->likes->count() }} {{ str_plural('like', $note->likes->count()) }}  </li>
							</ul>
							@foreach($note->replies as $reply)
								<div class="media">
									<a href="{{ route('profile.index', ['username' => $reply->user->username]) }}" class="pull-left">
										<img src="{{ $reply->user->getAvatarUrl() }}" alt="{{ $reply->user->getNameOrUsername() }}" class="media-object">
									</a>
									<div class="media-body">
										<h5 class="media-heading">
											<a href="{{ route('profile.index', ['username' => $reply->user->username]) }}">{{ $reply->user->getNameOrUsername() }}</a>
										</h5>
										<p> {{ $reply->note }} </p>
										<ul class="list-inline">
											<li>{{ $reply->created_at->diffForHumans() }}</li>
											@if($reply->user->_id !== Auth::user()->_id )
												<li><a href="{{ route('note.like', ['noteId' => $reply->_id]) }}">Like</a></li>
											@endif
											<li> {{ $reply->likes->count() }} {{ str_plural('like', $reply->likes->count()) }}  </li>
										</ul>
									</div>
								</div>
							@endforeach

							<form role="form" action="{{ route('note.reply', ['noteId' => $note->_id]) }}" method="post" >
								<div class="form-group {{ $errors->has("reply-{$note->_id}") ? 'has-error': '' }}">
									<textarea name="reply-{{$note->_id}}" id="" cols="30" rows="2" placeholder="Reply to this note" class="form-control"></textarea>
									@if($errors->has("reply-{$note->_id}"))
										<span class="help-block">{{ $errors->first("reply-{$note->_id}") }}</span>
									@endif
								</div>
								<input type="submit" value="Reply" class="btn btn-default btn-sm">
						    	{!! Form::token() !!}
							</form>
						</div>
					</div>
				@endforeach
				{!! $notes->render() !!}
				<br><br><br>
				 
			@endif
		</div>
	</div>

@stop