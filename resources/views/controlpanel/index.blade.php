@extends('templates.default')
@section('title')
Control Panel
@stop
@section('content')
	<h3>Control Panel</h3>
	<div class="row">
		<div class="col-md-3">
			{{-- User information --}}
			@include('user.partials.userblock')
			<hr>
			
			<!-- Nav tabs -->
			<ul class="nav nav-pills nav-stacked" role="tablist">
			    <li role="presentation" id="home-tab"  class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
			    <li role="presentation" id="" ><a href="#groups" aria-controls="groups" role="tab" data-toggle="tab">Manage groups</a></li>
			    <li role="presentation" id="" ><a href="#specialities" aria-controls="specialities" role="tab" data-toggle="tab">Manage specialities</a></li>
			    <li role="presentation" id="" ><a href="#users" aria-controls="users" role="tab" data-toggle="tab">Manage users</a></li>
			    <li role="presentation" id="" ><a href="#superiors" aria-controls="superiors" role="tab" data-toggle="tab">Manage superiors</a></li>
			    <li role="presentation" id="kanbantasks-tab" ><a href="#kanbantasks" aria-controls="kanbantasks" role="tab" data-toggle="tab">Manage kanban tasks</a></li>
			    <li role="presentation" id="" ><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
			    <li role="presentation" id="" ><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
		  	</ul>

		</div>
		<div class="col-md-9">
		  	<!-- Tab panes -->
		  	<div class="tab-content">
		    	<div role="tabpanel" class="tab-pane active" id="home">
					
		    	</div> {{-- End panel --}}


		    	{{-- --------------------- GROUPS --------------------- --}}
		    	<div role="tabpanel" class="tab-pane" id="groups">
		    		<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#create-group" aria-expanded="false" aria-controls="create-group">
					  	<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>	Group
					</button>
					<div class="collapse" id="create-group">
					  	<div class="well" style="margin-top:10px;">
				    		{{-- Form to create a new group --}}
				    		<form role="form" action=" {{ route('group.post') }} " method="post">
						    	<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} ">
									<label for="name" class="control-label">Create group</label>
									<input type="text" name="name" class="form-control" id="name" placeholder="Group Name" autofocus>
									@if($errors->has('name'))
										<span class="help-block">{{ $errors->first('name') }}</span>
									@endif
								</div>

								<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }} ">
									<textarea name="description" id="description" cols="30" rows="2" placeholder="Your group description here..." class="form-control"></textarea>
									@if($errors->has('description'))
										<span class="help-block">{{ $errors->first('description') }}</span>
									@endif
								</div>
								<button type="submit" class="btn btn-default">Accept</button>
								{!! Form::token() !!}

							</form>
					    
					  	</div>
					</div>
					<h3>Selecciona grupo para agregar integrantes</h3>
					<div class="row">
						<div class="col-md-4">
							
							{{-- Select a group --}}
							<ol id="selectable" class="ui-selectable selectable-group">
								@foreach($groups as $group)
									<li class="ui-state-default"> {{ $group->name}} </li>
								@endforeach
							</ol>
							

						</div>
						<div class="col-md-4">
							<div class="panel panel-default panel-users">
							  	<div class="panel-heading">Users</div>
							  	<div class="panel-body">
							    	<ol id="selectable" class="ui-selectable selectable-user">
										@foreach($users as $user)
											<li class="ui-state-default" data-token="{{ csrf_token() }}"> {{ $user->username}} </li>
										@endforeach
									</ol>
							  	</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="panel panel-default panel-users-group">
							  	<div class="panel-heading">Users in group</div>
							  	<div class="panel-body panel-body-users-group">
							    	<ol id="selectable" class="ui-selectable selectable-user-group">
										
							    	</ol>
							    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
							  	</div>
							</div>
						</div>
					</div>
		    	</div>
		    	{{-- ------------------ SPECIALITIES ------------------ --}}
		    	<div role="tabpanel" class="tab-pane" id="specialities">
		    		<div class="row">
		    		{{ Form::open(array('url' => 'speciality/add', 'method' => 'POST')) }}
						<div class="col-md-10">
							<div class="form-group {{ $errors->has('speciality') ? ' has-error' : '' }} ">
								<label for="title" class="control-label">Speciality name:</label>
								<input type="text" name="speciality" class="form-control" id="speciality" placeholder="Speciality..." autofocus>
								@if($errors->has('speciality'))
									<span class="help-block">{{ $errors->first('speciality') }}</span>
								@endif
							</div>
						</div>
						<div class="col-md-2">
								{{-- Element created to add space on top of button and align correctly with text input --}}
								<label>&nbsp;</label>
								<input type="submit" class="form-control btn btn-default" value="Accept" />
						</div>
					{{ Form::close() }}
					</div>
					<br>
					<div class="row">
						<div class="col-md-4">
				    		<div class="list-group">
								<a href="#" class="list-group-item active">Speciality</a>
					    		@foreach($specialities as $speciality)
							  		<a href="" class="list-group-item list-group-item-speciality" data-sp="{{$speciality->_id}}">{{$speciality->name}}</a>
					    		@endforeach	
				    		</div>
							
						</div>
						<div class="col-md-8 create-level-container">
							<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#create-level" aria-expanded="false" aria-controls="create-level">
							  	<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Level
							</button>
							<div class="collapse" id="create-level">
							  	<div class="well" style="margin-top:10px;">
						    		{{-- Form to create a new task --}}
						    		<form role="form" id="form-level-post" action=" {{ route('level.post') }} " method="post" enctype="multipart/form-data">
								    	{{-- Level Name --}}
										<div class="form-group {{ $errors->has('level_name') ? ' has-error' : '' }} ">
											<label for="title" class="control-label">Create level</label>
											<input type="text" name="level_name" class="form-control" id="level_name" placeholder="Level name" autofocus>
											@if($errors->has('level_name'))
												<span class="help-block">{{ $errors->first('level_name') }}</span>
											@endif
										</div>

										{{-- Points required --}}
										<div class="form-group {{ $errors->has('points_required') ? ' has-error' : '' }} ">
											<input type="text" name="points_required" class="form-control" id="points_required" placeholder="Points required" autofocus>
											@if($errors->has('points_required'))
												<span class="help-block">{{ $errors->first('points_required') }}</span>
											@endif
										</div>
										<label for="file" class="control-label">Upload image</label>
										<input type="file" name="fileToUpload" id="fileToUpload">
										<input type="hidden" name="MAX_FILE_SIZE" value="2097152">
										<input type="hidden" name="speciality_id" id="level_speciality_id" value="">
										{{Form::token()}}
										<hr>
										<button type="submit" class="btn btn-default" id="submit-level">Accept</button>

									</form>
							    
							  	</div>
							</div><!--End of collapse level -->
							<hr>
							
							
							<div class="panel panel-default">  
								<div class="panel-heading">Levels</div>  
								<table class="table table-level"> 
									<thead> 
										<tr> 
											<th>#</th> 
											<th>Name</th> 
											<th>Image</th> 
											<th>Points required</th> 
										</tr> 
									</thead> 
									<tbody class="tbody-level"> 
									</tbody> 
								</table> 
							</div><!--End of panel level -->
						</div>
					</div>
		    	</div> {{-- End panel --}}
	
		    	{{-- ------------------ USERS ------------------ --}}
		    	<div role="tabpanel" class="tab-pane" id="users">
		    		<div class="row">
		    			<div class="col-md-4">
							<div class="list-group">
								<a href="#" class="list-group-item active">Users</a>
					    		@foreach($users as $user)
							  		<a href="#" class="list-group-item list-group-item-user" data-us="{{$user->_id}}">{{$user->getNameOrUsername()}}</a>
					    		@endforeach	
				    		</div>
		    			</div>
		    			<div class="col-md-4">
							<div class="panel panel-primary panel-specialities-available"> 
								<div class="panel-heading"> <h3 class="panel-title">Available Specialities</h3> </div> 
								<div class="panel-body panel-body-specialities-available">
									<ol id="selectable" class="ui-selectable selectable-specialities-available">
							    	</ol>

								</div> 
							</div>
		    			</div>
		    			<div class="col-md-4">
							<div class="panel panel-primary panel-specialities-user"> 
								<div class="panel-heading"> <h3 class="panel-title">User Specialities</h3> </div> 
								<div class="panel-body panel-body-specialities-user">
									<ol id="selectable" class="ui-selectable selectable-specialities-user">
										
							    	</ol>
								</div> 
							</div>
		    			</div>
		    		</div>	
		    	</div>
		    	{{-- ------------------ SUPERIORS ------------------ --}}
		    	<div role="tabpanel" class="tab-pane" id="superiors">
		    		<div class="row">
		    			<div class="col-md-4">
							<div class="list-group">
								<a href="#" class="list-group-item active">Users</a>
					    		@foreach($users as $user)
							  		<a href="#" class="list-group-item list-group-item-superior" data-us="{{$user->_id}}">{{$user->getNameOrUsername()}}</a>
					    		@endforeach	
				    		</div>
		    			</div>
		    			<div class="col-md-4">
							<div class="panel panel-primary panel-subordinates-available"> 
								<div class="panel-heading"> <h3 class="panel-title">Available Users to Task</h3> </div> 
								<div class="panel-body panel-body-subordinates-available">
									<ol id="selectable" class="ui-selectable selectable-subordinates-available">
							    	</ol>

								</div> 
							</div>
		    			</div>
		    			<div class="col-md-4">
							<div class="panel panel-primary panel-subordinates"> 
								<div class="panel-heading"> <h3 class="panel-title">Users tasked</h3> </div> 
								<div class="panel-body panel-body-subordinates">
									<ol id="selectable" class="ui-selectable selectable-subordinates">
										
							    	</ol>
								</div> 
							</div>
		    			</div>
		    		</div>	
		    	</div>
		    	{{-- ------------------ KANBANTASKS ------------------ --}}
		    	<div role="tabpanel" class="tab-pane" id="kanbantasks">
		    		<div class="row">
						<div class="row">
							<div class="col-md-2"><b>Pending</b></div>
							<div class="col-md-3"><b>To-Do</b></div>
							<div class="col-md-3"><b>Today</b></div>
							<div class="col-md-2"><b>In-Progress</b></div>
							<div class="col-md-2"><b>Done</b></div>
						</div>
						<div class="row taskscontent">
							
						</div>
					</div>	
		    	</div>
		    	<div role="tabpanel" class="tab-pane" id="messages">4...</div>
		    	<div role="tabpanel" class="tab-pane" id="settings">...4</div>
		 	</div>
		</div>
	</div>


	<div class="modal fade" id="doneTaskModal" tabindex="-1" role="dialog" aria-labelledby="doneTaskModalLabel">
		<div class="modal-dialog" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title" id="doneTaskModalLabel">New message</h4>
		      	</div>
		      	<div class="modal-body">
		        	<form>
		          		<div class="form-group">
		            		<label for="description" class="control-label">Description:</label>
		            		<textarea class="form-control" id="description" rows="5"  style="resize: vertical;" readonly></textarea>
		          		</div>
		          		

						<input type="hidden" name="id" id="task-id">
		        	</form>
		      	</div>
		      	<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        	<button type="button" class="btn btn-primary" id="task-modal-realese">Realese</button>
		      	</div>
		    </div>
		</div>
	</div>

@stop