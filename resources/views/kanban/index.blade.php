@extends('templates.default')
@section('title')
Control Panel
@stop
@section('content')
	
		<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#create-group" aria-expanded="false" aria-controls="create-group">
		  	<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Task
		</button>
		<a href="/kanban/released" class="btn btn-primary pull-right">
		  	<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Released
		</a>
		<div class="collapse" id="create-group">
		  	<div class="well" style="margin-top:10px;">
	    		{{-- Form to create a new task --}}
	    		<form role="form" id="form-task-post" action=" {{ route('task.post') }} " method="post">
			    	<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} ">
						<label for="name" class="control-label">Create task</label>
						<input type="text" name="name" class="form-control" id="name" placeholder="Task Name" value="{{ Request::old('name') ? : '' }}" autofocus>
						@if($errors->has('name'))
							<span class="help-block">{{ $errors->first('name') }}</span>
						@endif
					</div>


					<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }} ">
						<textarea name="description" id="description" cols="30" rows="2" placeholder="Your task description here..." class="form-control">{{ Request::old('description') ? : '' }}</textarea>
						@if($errors->has('description'))
							<span class="help-block">{{ $errors->first('description') }}</span>
						@endif
					</div>
					Tag:
					<div class="form-group {{ $errors->has('tags') ? ' has-error' : '' }} ">
						{!!  Form::select('tags[]', $tags, null, ['id' => 'tag_list', 'class' => 'form-control', 'multiple', 'style' => 'width:100%;']) !!}
						@if($errors->has('tags'))
							<span class="help-block">{{ $errors->first('tags') }}</span>
						@endif
					</div>

					


					<!-- Single button -->
					<div class="form-group {{ $errors->has('color') ? ' has-error' : '' }} {{ $errors->has('start_date') ? ' has-error' : '' }} {{ $errors->has('due_date') ? ' has-error' : '' }} ">
						Color Tag:
						<div class="btn-group" >
							{{-- <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} ">
							</div> --}}
						    <button  type="button" id="select-color-btn" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						      	<span id="color-btn-text" class="dropdown-menu-text">Color</span>
						      	<span class="caret"></span>
						    </button>
						    <ul class="dropdown-menu" id="select-color-ul">
					    		<li class="dropdown-colorname dropdown-item"><a>Red</a></li>
					    		<li class="dropdown-colorname dropdown-item"><a>Blue</a></li>
					    		<li class="dropdown-colorname dropdown-item"><a>Green</a></li>
					    		<li class="dropdown-colorname dropdown-item"><a>Yellow</a></li>
					    		<li class="dropdown-colorname dropdown-item"><a>Gray</a></li>
					    		<li class="dropdown-colorname dropdown-item"><a>Purple</a></li>
					    		<li class="dropdown-colorname dropdown-item"><a>Pink</a></li>
					    		<li class="dropdown-colorname dropdown-item"><a>Orange</a></li>
					    		<li class="dropdown-colorname dropdown-item"><a>No Color</a></li>
						    </ul>
					  	</div>
					  	<input type="hidden" id="input-hidden-color" class="input-hiddem-dropdown-item" name="color" value="No Color"></input>
						<div class="btn-group" >
							<div>Start Date: <input type="text" name="start_date" value="" class="datepicker"></div>
					  	</div>
						<div class="btn-group">
							<div>Due Date: <input type="text" name="due_date" value="" class="datepicker"></div>
						</div>
						@if($errors->has('color'))
							<span class="help-block">{{ $errors->first('color') }}</span>
						@endif
						@if($errors->has('start_date'))
							<span class="help-block">{{ $errors->first('start_date') }}</span>
						@endif
						@if($errors->has('due_date'))
							<span class="help-block">{{ $errors->first('due_date') }}</span>
						@endif
						<div class="btn-group" >
							<div>Editable: <input id="editabletask-checkbox" name="editabletask" class="form-control" type="checkbox" checked></div>
					  	</div>
						
					</div>
					

					Responsible(s):
					<div class="form-group {{ $errors->has('subordinates') ? ' has-error' : '' }} ">
						{!!  Form::select('subordinates[]', $subordinates, null, ['id' => 'user_list', 'class' => 'form-control', 'multiple', 'style' => 'width:100%;']) !!}
						@if($errors->has('subordinates'))
							<span class="help-block">{{ $errors->first('subordinates') }}</span>
						@endif
					</div>

					<hr>
					<button type="submit" class="btn btn-default" id="submit-task">Accept</button>
					{!! Form::token() !!}

				</form>
		    
		  	</div>
		</div>

		
		<hr>
{{-- ************************************************************************************************** --}}
{{-- ****************************************** KANBAN BOARD ****************************************** --}}
{{-- ************************************************************************************************** --}}
		<div class="row">
			<div class="col-lg-2 column-kanban" >
				<div class="panel panel-default panel-column-kanban " >
				  	<div class="panel-heading"><b>Pending</b></div>
				  	<div class="panel-body panel-body-pending" style="height:90%;">
						{{-- Select a task 
				    	<ol id="selectable" class="ui-selectable selectable-user">
							@foreach($subordinates as $user)
								<li class="ui-state-default" data-token="{{ csrf_token() }}"> {{ $user->username}} </li>
							@endforeach
						</ol>
						--}}
						@foreach($tasksPending as $task)
						    <li class="panel panel-default draggable-task" data-toggle="modal" data-target="#taskModal" data-id="{{$task->_id}}" data-color="{{$task->color}}" data-due_date="{{$task->due_date}}">
						        <div class="panel-heading panel-heading-task scrollEffectText" style="background-color:{{$task->color}};" >
									<span class="overflowContent h4" style="margin-left: 0px;">{{$task->name}}</span>
								</div>
						        <div class="panel-body">{{$task->description}}</div>
						    </li>
						@endforeach
				  	</div>
				</div>
			</div>
			
			<div class="col-lg-3 column-kanban" >
				<div class="panel panel-default panel-column-kanban ">
				  	<div class="panel-heading"><b>To-Do</b></div>
				  	{{-- height of 90% was applied to increase area of droppable when div panel-body is empty --}}
				  	<div class="panel-body panel-body-todo" style="height:90%;"> 
					  	@foreach($tasksTodo as $task)
						    <li class="panel panel-default draggable-task" data-toggle="modal" data-target="#taskModal" data-id="{{$task->_id}}" data-color="{{$task->color}}" data-due_date="{{$task->due_date}}">
						        <div class="panel-heading panel-heading-task scrollEffectText" style="background-color:{{$task->color}};">
									<span class="overflowContent h4" style="margin-left: 0px;">{{$task->name}}</span>
								</div>
						        <div class="panel-body">{{$task->description}}</div>
						    </li>
						@endforeach

				  	</div>
				</div>		
			</div>
			<div class="col-lg-3 column-kanban">
				<div class="panel panel-default panel-column-kanban ">
				  	<div class="panel-heading"><b>Today</b></div>
				  	<div class="panel-body panel-body-today" style="height:90%;">

					    @foreach($tasksToday as $task)
						    <li class="panel panel-default draggable-task" data-toggle="modal" data-target="#taskModal" data-id="{{$task->_id}}" data-color="{{$task->color}}" data-due_date="{{$task->due_date}}">
						        <div class="panel-heading panel-heading-task scrollEffectText" style="background-color:{{$task->color}};">
									<span class="overflowContent h4" style="margin-left: 0px;">{{$task->name}}</span>
								</div>
						        <div class="panel-body">{{$task->description}}</div>
						    </li>
						@endforeach

				  	</div>
				</div>
			</div>
			<div class="col-lg-2 column-kanban">
				<div class="panel panel-default panel-column-kanban">
				  	<div class="panel-heading"><b>In-Progress</b></div>
				  	<div class="panel-body panel-body-inprogress" style="height:90%;">
				    	@foreach($tasksInprogress as $task)
						    <li class="panel panel-default draggable-task" data-toggle="modal" data-target="#taskModal" data-id="{{$task->_id}}" data-color="{{$task->color}}" data-due_date="{{$task->due_date}}">
						        <div class="panel-heading panel-heading-task scrollEffectText" style="background-color:{{$task->color}};">
									<span class="overflowContent h4" style="margin-left: 0px;">{{$task->name}}</span>
								</div>
						        <div class="panel-body">{{$task->description}}</div>
						    </li>
						@endforeach	
				  	</div>
				</div>
			</div>

			<div class="col-lg-2 column-kanban">
				<div class="panel panel-default panel-column-kanban">
				  	<div class="panel-heading"><b>Done</b></div>
				  	<div class="panel-body panel-body-done" style="height:90%;">
				    	@foreach($tasksDone as $task)
						    <li class="panel panel-default draggable-task" data-toggle="modal" data-target="#taskModal" data-id="{{$task->_id}}" data-color="{{$task->color}}" data-due_date="{{$task->due_date}}">
						        <div class="panel-heading panel-heading-task scrollEffectText" style="background-color:{{$task->color}};">
									<span class="overflowContent h4" style="margin-left: 0px;">{{$task->name}}</span>
								</div>
						        <div class="panel-body">{{$task->description}}</div>
						    </li>
						@endforeach	
				  	</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="taskModal" tabindex="-1" role="dialog" aria-labelledby="taskModalLabel">
		<div class="modal-dialog" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title" id="taskModalLabel">New message</h4>
		      	</div>
		      	<div class="modal-body">
		        	<form>
		          		<div class="form-group">
		            		<label for="description" class="control-label">Description:</label>
		            		<textarea class="form-control" id="description" rows="5"  style="resize: vertical;"></textarea>
		          		</div>
		          		<div class="form-group">
		            		<label for="task-color" class="control-label">Color:</label>
		            		
			          		<div class="btn-group">
							    <button type="button" id="select-color-btn" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
					      			<span id="color-btn-text">No Color</span>
							      	
							      	<span class="caret"></span>
							    </button>
							    <ul class="dropdown-menu" id="select-color-ul">
						    		<li class="dropdown-colorname"><a>Red</a></li>
						    		<li class="dropdown-colorname"><a>Blue</a></li>
						    		<li class="dropdown-colorname"><a>Green</a></li>
						    		<li class="dropdown-colorname"><a>Yellow</a></li>
						    		<li class="dropdown-colorname"><a>Gray</a></li>
						    		<li class="dropdown-colorname"><a>Purple</a></li>
						    		<li class="dropdown-colorname"><a>Pink</a></li>
						    		<li class="dropdown-colorname"><a>Orange</a></li>
						    		<li class="dropdown-colorname"><a>No Color</a></li>
							    </ul>
						  	</div>
		          		</div>

		          		<div class="form-group">
		            		<label for="task-duedate" class="control-label">Due date:</label>
					  		<div class="btn-group">
								<input type="text" name="due_date" value="" class="form-control datepicker">
							</div>
						</div>
						<input type="hidden" name="id" id="task-id">
		        	</form>
		      	</div>
		      	<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        	<button type="button" class="btn btn-primary" id="task-modal-submit">Accept</button>
		      	</div>
		    </div>
		</div>
	</div>	
@stop     