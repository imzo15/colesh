@extends('templates.default')
@section('title')
Released tasks
@stop
@section('content')
	
	<div class="row">
		<div class="col-lg-6">
			@if(count($tasks))
				<label for="released-tasks" class="control-label">Released tasks</label>
			@else
				<span class="h4">No Released tasks</span> 
			@endif
			@foreach($tasks as $task)
				<a href="" class="task-panel">
					<div class="panel panel-info task-panel-info"> 
						<div class="panel-heading"> <h3 class="panel-title">{{ $task->name }}</h3> </div> 
						<div class="panel-body">{{ $task->description }}</div> 
					</div>
				</a>
			@endforeach


		</div>
		<div class="col-lg-6">
			
			
		</div>
	</div>
	
@stop     