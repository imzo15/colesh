@extends('templates.default')
@section('title')
Im following
@stop
@section('content')
	
	<!-- Bug: This is the only page where you need to add this top margin -->
	<div style="margin-top:60px;" ></div>
	<h3>Follow</h3>
	<div class="row">
		{{-- List of people you follow --}}
		<div class="col-lg-3">
			<h4>Following</h4>
			@if(!$following->count())
				<p>You are not following anybody.</p>
			@else
				 @foreach( $following as $user )
				 	@include('user/partials/userblock')
				 @endforeach
			@endif
		</div> 
		{{-- List of people who are following you --}}
		<div class="col-lg-3">
			<h4>Followed By</h4>
			
			@if(!$followers->count())
				<p>Nobody is following you.</p>
			@else
				 @foreach( $followers as $user )
				 	@include('user/partials/userblock')
				 @endforeach
			@endif
			
		</div>
		{{-- List of people who you have sent follow request --}}
		<div class="col-lg-3">
			<h4>Requests sent pending</h4>
			@if(!$followRequestsSentPending->count())
				<p>You have no sent requests pending to accept.</p>
			@else
				 @foreach( $followRequestsSentPending as $user )
				 	@include('user/partials/userblock')
				 @endforeach
			@endif
		</div>
		{{-- List of people who have sent you a follow request--}}
		<div class="col-lg-3">
			<h4>Requests received pending</h4>
			@if(!$followRequestsReceivedPending->count())
				<p>You have no requests pending to accept.</p>
			@else
				 @foreach( $followRequestsReceivedPending as $user )
				 	@include('user/partials/userblock')
				 @endforeach
			@endif
		</div>

	</div>

@stop 