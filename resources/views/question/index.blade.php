@extends('templates.default')
@section('title')
Question
@stop
@section('content')
	<div class="media">
		<a href="{{ route('profile.index', ['username' => $question->user->username]) }}" class="pull-left">
			<img src="{{ $question->user->getAvatarUrl() }}" alt=" {{ $question->user->getNameOrUsername() }} " class="media-object">
		</a>
		<div class="media-body">
			<h4 class="media-heading"><a href="{{ route('profile.index', ['username' => $question->user->username]) }}">{{$question->user->getNameOrUsername()}}</a></h4>
			<p>
				<h3>{{ $question->title}}</h3>
				{{ $question->description }}
			</p>
			<ul class="list-inline">
				<li>{{ $question->created_at->diffForHumans() }}</li>
				@if($question->user->_id !== Auth::user()->_id )
					<li><a href="{{ route('question.index', ['questionId' => $question->_id]) }}">Like</a></li>
				@endif
				<li> 2 likes</li>
				{{--  
				<li> {{ $question->likes->count() }} {{ str_plural('like', $question->likes->count()) }}  </li>
				 --}}
			</ul>

		</div>

		@foreach($question->images as $image)
			<div>
				<img src="/{{$image->path}}/thumb_{{$image->name}}.{{$image->extension}}" alt="">
			</div>
		@endforeach

	</div>

	<form role="form" action=" {{ route('answer.post') }} " method="post">
		<label for="title-question" class="control-label">Answer question</label>
	
		<div class="form-group {{ $errors->has('answer') ? ' has-error' : '' }} ">
			<textarea name="answer" id="answer" cols="30" rows="2" placeholder="Your answer here {{ Auth::user()->getFirstNameOrUsername() }}..." class="form-control"></textarea>
			@if($errors->has('answer'))
				<span class="help-block">{{ $errors->first('answer') }}</span>
			@endif
		</div>
		<input type="hidden" name="question_id" value="{{$question->_id}}">
		<button type="submit" class="btn btn-default pull-right">Answer</button>
		{!! Form::token() !!}
	
	</form>
	<br><br>
	@foreach($question->answers as $answer)
		<br>
		<div class="panel panel-default">
		  	<div class="panel-heading">
				<div class="row">
					<div class="col-md-10">
						<h3 class="panel-title">{{$answer->user->getNameOrUsername()}}</h3>
					</div>
					<div class="col-md-2">
						<span class="pull-right">Speciality</span>
					</div>
				</div>
		  	</div>
		  	<div class="panel-body">
				{{$answer->answer}}
		  	</div>
		  	@if(count($answer->images))
	  		<div class="panel-footer">
				@foreach($answer->images as $image)
					<div class="answer-image-thumb">
						<img src="/{{$image->path}}/thumb_{{$image->name}}.{{$image->extension}}" alt="{{$image->title}}" data-toggle="modal" data-target="#answerImageModal" data-alttitle="{{$answer->title}}" data-title="{{$image->title}}" data-path="{{$image->path}}" data-name="{{$image->name}}" data-extension="{{$image->extension}}">
					</div>
				@endforeach
	  		</div>
	  		@endif
	  		@forelse($answer->myRating() as $rating)
				<div class="pull-right">
					<div class="rate rate-answer" ro="true" data-score="{{$rating->rating}}" data-id="{{$answer->_id}}" data-token="{{ csrf_token() }}"></div>
				</div>
			@empty
				@if($answer->user_id != Auth::user()->_id)
				<div class="pull-right">
					<div class="rate rate-answer" data-score="" data-id="{{$answer->_id}}" data-token="{{ csrf_token() }}"></div>
				</div>
				@endif
	  		@endforelse
		</div>
	@endforeach

	
@stop


{{-- 
<div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="h2">NBA Playoffs 2016</div>
                <div class="brackets" id="brackets">
                    <div class="group5" id="b0">
                        <div class="r1">
                            <div>
                                <div class="bracketbox">
                                    <span class="teama">
                                        <img src="img/CLE.jpg" alt="" width="20" height="20" style="padding-bottom:1px;">
                                        <span style="font-weight:bold;">1. Cleveland Cavaliers</span>
                                        &nbsp;&nbsp;
                                        <span class="label label-success">4</span>
                                    </span>
                                    <span class="teamb">
                                        <img src="img/DET.jpg" alt="" width="20" height="20" style="padding-bottom:1px;">
                                        <span style="font-weight:bold;">8. Detroit Pistons</span>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span class="label label-danger">0</span>
                                        
                                    </span>
                                </div>
                            </div>
                            <div>
                                <div class="bracketbox">
                                    <span class="teama">Detroit Red Wings</span>
                                    <span class="teamb">Calgary Flames</span>
                                </div>
                            </div>
                            <div>
                                <div class="bracketbox">
                                    <span class="teama">Chicago Blackhawks</span>
                                    <span class="teamb">New York Rangers</span>
                                </div>
                            </div>
                            <div>
                                <div class="bracketbox">
                                    <span class="teama">Toronto Maple Leafs</span>
                                    <span class="teamb">Carolina Hurricanes</span>
                                </div>
                            </div>
                            <div>
                                <div class="bracketbox">
                                    <span class="teama">Anaheim Ducks</span>
                                    <span class="teamb">Philadelphia Flyers</span>
                                    </div>
                                </div>
                            <div>
                                <div class="bracketbox">
                                    <span class="teama">Nashville Predators</span>
                                    <span class="teamb">Quebec Nordiques</span>
                                </div>
                            </div>
                            <div>
                                <div class="bracketbox">
                                    <span class="teama">Buffalo Sabres</span>
                                    <span class="teamb">Montreal Wanderers</span>
                                </div>
                            </div>
                            <div>
                                <div class="bracketbox">
                                    <span class="teama">Washington Capitals</span>
                                    <span class="teamb">Colorado Avalanche</span>
                                </div>
                            </div>
                        </div>
                        <div class="r2">
                            <div>
                                <div class="bracketbox">
                                    <span class="teama">Boston Bruins</span>
                                    <span class="teamb">Detroit Red Wings</span>
                                </div>
                            </div>
                            <div>
                                <div class="bracketbox">
                                    <span class="teama">New York Rangers</span>
                                    <span class="teamb">Toronto Maple Leafs</span>
                                </div>
                            </div>
                            <div>
                                <div class="bracketbox">
                                    <span class="teama">Philadelphia Flyers</span>
                                    <span class="teamb">Nashville Predators</span>
                                </div>
                            </div>
                            <div>
                                <div class="bracketbox">
                                    <span class="teama">Buffalo Sabres</span>
                                    <span class="teamb">Colorado Avalanche</span>
                                </div>
                            </div>
                        </div>
                        <div class="r3">
                            <div>
                                <div class="bracketbox">
                                    <span class="teama">Detroit Red Wings</span>
                                    <span class="teamb">Toronto Maple Leafs</span>
                                </div>
                            </div>
                            <div>
                               <div class="bracketbox">
                                    <span class="teama">Philadelphia Flyers</span>
                                    <span class="teamb">Buffalo Sabres</span>
                                </div>
                            </div>
                        </div>
                        <div class="r4">
                            <div>
                                <div class="bracketbox">
                                    <span class="info">15</span>
                                    <span class="teama">Detroit Red Wings</span>
                                    <span class="teamb">Buffalo Sabres</span>
                                </div>
                            </div>
                        </div>
                        <div class="r5">
                            <div class="final">
                                <div class="bracketbox">
                                    <span class="teamc">Detroit Red Wings</span>
                                </div>
                            </div>
                        </div>
                    </div><!-- End of group -->
                </div><!-- End of bracket -->
            </div> <!-- End of col-md-10 -->
            <div class="col-md-1"></div>
        </div>
    </div>



    <style type="text/css">
			html, body, .brackets {
				width: 100%;
				min-height: 100%;
				font-family: "Arial", sans-serif;
			}
			.metroBtn {
				background-color: #2E7BCC;
				color: #fff;
				font-size: 1.1em;
				padding: 10px;
				display: inline-block;
				margin-bottom: 30px;
				cursor: pointer;
			}
			.brackets > div {
				vertical-align: top;
				clear: both;
			}
			.brackets > div > div {
				float: left;
				height: 100%;
			}
			.brackets > div > div > div {
				margin: 50px 0;
			}
			.brackets div.bracketbox {
				position: relative;
				width: 100%; height: 100%;
				border-top: 1px solid #555;
				border-right: 1px solid #555;
				border-bottom: 1px solid #555;
			}
			.brackets div.bracketbox > span.info {
				position: absolute;
				top: 25%;
				left: 25%;
				font-size: 0.8em;
				color: #BBB;
			}
			.brackets div.bracketbox > span {
				position: absolute;
				left: 5px;
				font-size: 0.85em;
			}
			.brackets div.bracketbox > span.teama {
				top: -20px;
			}
			.brackets div.bracketbox > span.teamb {
				bottom: -20px;
			}
			.brackets div.bracketbox > span.teamc {
				bottom: 1px;
			}
			.brackets > .group2 {
				height: 260px;
			}
			.brackets > .group2 > div {
				width: 49%;
			}
			.brackets > .group3 {
				height: 320px;
			}
			.brackets > .group3 > div {
				width: 32.7%;
			}
			.brackets > .group4 > div {
				width: 24.5%;
			}
			.brackets > .group5 > div {
				width: 19.6%;
			}
			.brackets > .group6 {
				height: 2000px;
			}
			.brackets > .group6 > div {
				width: 16.3%;
			}
			.brackets > div > .r1 > div {
				height: 60px;
			}
			.brackets > div > .r2 > div {
				margin: 80px 0 110px 0;
				height: 110px;
			}
			.brackets > div > .r3 > div {
				margin: 135px 0 220px 0;
				height: 220px;
			}
			.brackets > div > .r4 > div {
				margin: 250px 0 445px 0;
				height: 445px;
			}
			.brackets > div > .r5 > div {
				margin: 460px 0 0 0;
				height: 900px;
			}
			.brackets > div > .r6 > div {
				margin: 900px 0 0 0;
			}
			.brackets div.final > div.bracketbox {
				border-top: 0px;
				border-right: 0px;
				height: 0px;
			}
			.brackets > div > .r4 > div.drop {
				height: 180px;
				margin-bottom: 0px;
			}
			.brackets > div > .r5 > div.final.drop {
				margin-top: 345px;
				margin-bottom: 0px;
				height: 1px;
			}
			.brackets > div > div > div:last-of-type {
				margin-bottom: 0px;
			}
		</style>

		
 --}}
