@extends('templates.default')
@section('title')
Question
@stop
@section('content')
	<br>

	<form role="form" action=" {{ route('question.post') }} " method="post" enctype="multipart/form-data">
    	<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }} ">
			<label for="title" class="control-label">Ask a question</label>
			<input type="text" name="title" class="form-control" id="title" placeholder="Title question" autofocus>
			@if($errors->has('title'))
				<span class="help-block">{{ $errors->first('title') }}</span>
			@endif
		</div>
	
		<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }} ">
			<textarea name="description" id="description" cols="30" rows="2" placeholder="Your description here {{ Auth::user()->getFirstNameOrUsername() }}..." class="form-control"></textarea>
			@if($errors->has('description'))
				<span class="help-block">{{ $errors->first('description') }}</span>
			@endif
		</div>
		Speciality:
		<div class="form-group {{ $errors->has('speciality') ? ' has-error' : '' }} ">
			<div class="btn-group" >
			    <button  type="button" id="select-speciality-btn" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
			      	<span id="speciality-btn-text" class="dropdown-menu-text">speciality</span>
			      	<span class="caret"></span>
			    </button>
			    <ul class="dropdown-menu" id="select-speciality-ul">
			    	@foreach($specialities as $speciality)
		    			<li class="dropdown-specialityname dropdown-item"><a>{{$speciality->name}}</a></li>
		    		@endforeach
			    </ul>
			</div>
			@if($errors->has('speciality'))
				<span class="help-block">{{ $errors->first('speciality') }}</span>
			@endif
	  	</div>
	  	<input type="hidden" id="input-hidden-speciality" class="input-hiddem-dropdown-item" name="speciality" value="Speciality"></input>
		

		@if(session('fileToUploadErrorSize'))
			<div class="form-group has-error">
		@else
			<div class="form-group {{ $errors->has('fileToUpload') ? ' has-error' : '' }} ">
		@endif
			<input type="file" name="fileToUpload" id="fileToUpload">
			<input type="hidden" name="MAX_FILE_SIZE" value="2097152">
			{{-- This if was made to catch custom error UPLOAD_ERR_INI_SIZE in QuestionController --}}
			@if($errors->has('fileToUpload') || session('fileToUploadErrorSize'))
				<span class="help-block">{{ $errors->first('fileToUpload') == null ? Session::get('fileToUploadErrorSize') : $errors->first('fileToUpload') }}</span>
			@endif
		</div>

		<button type="submit" class="btn btn-default">Post Question</button>
		{!! Form::token() !!}
	
	</form>
	<hr>
	
	@forelse($questions as $question)
	
		<div class="panel panel-default" >
			<a href="{{ route('question.index', ['id' => $question->_id]) }}" class="question-panel">
	  		<div class="panel-body question-results-panel-body">
				<h3 class="question-results-title">{{$question->title}}</h3>			
					
				<ul class="list-inline">
					<li>{{$question->description}}</li>
					<hr class="question-results-hr">
					<li>Created: {{ $question->created_at->diffForHumans() }}</li> by {{ $question->user->getNameOrUsername() }}
					<span class="label label-default">
						{{ $question->answers->count() }} {{ str_plural('answer', $question->answers->count()) }}
					</span>
				</ul>
	  		</div>
			</a>	
	  		@if(count($question->images))
	  		<div class="panel-footer">
				@foreach($question->images as $image)
					<div class="question-image-thumb">
						<img src="/{{$image->path}}/thumb_{{$image->name}}.{{$image->extension}}" alt="{{$image->title}}" data-toggle="modal" data-target="#questionImageModal" data-alttitle="{{$question->title}}" data-title="{{$image->title}}" data-path="{{$image->path}}" data-name="{{$image->name}}" data-extension="{{$image->extension}}">
					</div>
				@endforeach
	  		</div>
	  		@endif
		</div>

		

	@empty
		No results found!
	@endforelse
	{{-- Modal for image  --}}
		<div class="modal fade" id="questionImageModal" tabindex="-1" role="dialog" aria-labelledby="questionImageLabel" >
			<div class="modal-dialog" role="document" style="width:80%;">
			    <div class="modal-content">
			      	<div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        	<h4 class="modal-title" id="questionImageLabel">Question</h4>
			      	</div>
			      	<div class="modal-body">
			        <img id="questionImageimg" src="" alt="" max-width="50%">	
			      	</div>
			      	<div class="modal-footer">
			        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        	<button type="button" class="btn btn-primary" id="task-modal-submit">Accept</button>
			      	</div>
			    </div>
			</div>
		</div>
@stop