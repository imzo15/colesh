<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Sidebar - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <style>
        /*!
 * Start Bootstrap - Simple Sidebar HTML Template (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

/* Toggle Styles */

#wrapper {
    padding-right: 0;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}

#wrapper.toggled {
    padding-right: 250px;
}

#sidebar-wrapper {
    z-index: 1;
    position: fixed;
    right: 250px;
    width: 0;
    height: 100%;
    margin-right: -250px;
    /*overflow-y: auto;
    overflow-x: visible;
    */
    background: #000;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}

#wrapper.toggled #sidebar-wrapper {
    width: 250px;
}

#page-content-wrapper {
    width: 100%;
    position: absolute;
    padding: 15px;
}

#wrapper.toggled #page-content-wrapper {
    position: absolute;
    margin-right: -250px;
}

/* Sidebar Styles */

.sidebar-nav {
    position: absolute;
    top: 0;
    width: 250px;
    margin: 0;
    padding: 0;
    list-style: none;
}

.sidebar-nav li {
    text-indent: 20px;
    line-height: 40px;
}

.sidebar-nav li a {
    display: block;
    text-decoration: none;
    color: #999999;
}

/*.sidebar-nav li a:hover {
    text-decoration: none;
    color: #fff;
    background: rgba(255,255,255,0.2);
}*/

.sidebar-nav li a:active,
.sidebar-nav li a:focus {
    text-decoration: none;
}

.sidebar-nav > .sidebar-brand {
    height: 65px;
    font-size: 18px;
    line-height: 60px;
}

.sidebar-nav > .sidebar-brand a {
    color: #999999;
}

/*.sidebar-nav > .sidebar-brand a:hover {
    color: #fff;
    background: none;
}
*/
@media(min-width:768px) {
    #wrapper {
        padding-right: 250px;
    }

    #wrapper.toggled {
        padding-right: 0;
    }

    #sidebar-wrapper {
        width: 250px;
    }

    #wrapper.toggled #sidebar-wrapper {
        width: 0;
    }

    #page-content-wrapper {
        padding: 20px;
        position: relative;
    }

    #wrapper.toggled #page-content-wrapper {
        position: relative;
        margin-right: 0;
    }
}


@import url(http://fonts.googleapis.com/css?family=Oxygen+Mono);
/* Please Keep this font import at the very top of any CSS file */
@charset "UTF-8";
/* Starter CSS for Flyout Menu */
.cssmenu {
  padding: 0;
  margin: 0;
  border: 0;
  line-height: 1;
}
.cssmenu ul.menucontainer,
.cssmenu ul.menucontainer li.menuitem,
.cssmenu ul.menucontainer ul.submenucontainer {
  list-style: none;
  margin: 0;
  padding: 0;
}
.cssmenu ul.menucontainer {
  position: relative;
  z-index: 1500;
  float: right;
}
.menuitem,
.submenuitem {
  float: right;
  min-height: 1px;
  line-height: 1em;
  vertical-align: middle;
  position: relative;
}
/*.cssmenu ul li.hover,
.cssmenu ul li:hover {
  position: relative;
  z-index: 599;
  cursor: default;
}*/
/* Modificar este para hacer un acordeon */
.submenucontainer{
  visibility: hidden;
  position: absolute;
  top: 100%;
  right: 0px;
  z-index: 598;
  width: 100%;
}
.submenuitem {
  float: none;
}
/*.cssmenu ul ul ul {
  top: -2px;
  right: 0;
}*/
/*.cssmenu ul li:hover > ul {
  visibility: visible;
}*/
.submenucontainer {
  top: 1px;
  right: 99%;
}
.menuitem {
  float: none;
}
.submenucontainer {
  margin-top: 1px;
}
.submenuitem {
  font-weight: normal;
}
/* Custom CSS Styles */
.cssmenu {
  width: 200px;
  background: #333333;
  font-family: 'Oxygen Mono', Tahoma, Arial, sans-serif;
  zoom: 1;
  font-size: 12px;
}
.cssmenu:before {
  content: '';
  display: block;
}
.cssmenu:after {
  content: '';
  display: table;
  clear: both;
}
.menutitle,
.submenutitle {
  display: block;
  padding: 15px 20px;
  color: #ffffff;
  text-decoration: none;
  text-transform: uppercase;
}
.cssmenu > ul {
  width: 100%;
}
.submenucontainer {
  width: 350px;
}
.cssmenu > ul > li > a {
  border-right: 4px solid #337ab7;
  color: #ffffff;
}
/*.cssmenu > ul > li > a:hover {
  color: #ffffff;
}*/
.cssmenu > ul > li.subactive a {
  background: #337ab7;
}
.cssmenu > ul > li.active a {
  background: #337ab7;
}
/*.cssmenu > ul > li a:hover,
.cssmenu > ul > li:hover a {
  background: #337ab7;
}*/
.menuitem,
.submenuitem {
  position: relative;
}
.online-status{
    content: ' ';
    position: absolute;
    border-radius: 50%;
    width: 10px;
    height: 10px;
    top: 50%;
    right: 15px;
    margin-top: -6px;
}
.cssmenu ul li.online > .online-status{
  background-color: #0CA80E;
}
.cssmenu ul li.offline > .online-status{
  background-color: #A80C21;
}

.cssmenu ul ul li.first {
  -webkit-border-radius: 0 3px 0 0;
  -moz-border-radius: 0 3px 0 0;
  border-radius: 0 3px 0 0;
}
.cssmenu ul ul li.last {
  -webkit-border-radius: 0 0 3px 0;
  -moz-border-radius: 0 0 3px 0;
  border-radius: 0 0 3px 0;
  border-bottom: 0;
}
.submenucontainer {
  -webkit-border-radius: 0 3px 3px 0;
  -moz-border-radius: 0 3px 3px 0;
  border-radius: 0 3px 3px 0;
}
/*.submenucontainer{
  border: 1px solid #0082e7;
}*/
.submenutitle{
  font-size: 12px;
  color: #ffffff;
}
/*.cssmenu ul ul a:hover {
  color: #ffffff;
}*/
/*.submenuitem{
  border-bottom: 1px solid #0082e7;
}*/
/*.cssmenu ul ul li:hover > a {
  background: #4eb1ff;
  color: #ffffff;
}*/
.cssmenu.align-right > ul > li > a {
  border-right: 4px solid #337ab7;
  border-right: none;
}
.cssmenu.align-right {
  float: right;
}
.cssmenu.align-right li {
  text-align: right;
}
.cssmenu.align-right ul li.has-sub > a:before {
  content: '+';
  position: absolute;
  top: 50%;
  right: 15px;
  margin-top: -6px;
}
.cssmenu.align-right ul li.has-sub > a:after {
  content: none;
}
.cssmenu.align-right ul ul {
  visibility: hidden;
  position: absolute;
  top: 0;
  right: -100%;
  z-index: 598;
  width: 100%;
}
.cssmenu.align-right ul ul li.first {
  -webkit-border-radius: 3px 0 0 0;
  -moz-border-radius: 3px 0 0 0;
  border-radius: 3px 0 0 0;
}
.cssmenu.align-right ul ul li.last {
  -webkit-border-radius: 0 0 0 3px;
  -moz-border-radius: 0 0 0 3px;
  border-radius: 0 0 0 3px;
}
.cssmenu.align-right ul ul {
  -webkit-border-radius: 3px 0 0 3px;
  -moz-border-radius: 3px 0 0 3px;
  border-radius: 3px 0 0 3px;
}


.menuheadertitle {
    display: block;
    padding: 15px 20px;
    color: #ffffff;
    text-decoration: none;
    text-transform: uppercase;
    background: #337ab7;
}
.menuheadertitle:hover,
.menutitle:hover,
.submenutitle:hover{
  text-decoration: none;
}

.menutitle,
.submenutitle{
  text-decoration: none;
}

.menutitle:link,
.submenutitle:link{
  text-decoration: none;
}

    </style>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    </head>

    <body>

 

    <div id="wrapper">

        <div id="sidebar-wrapper" class="cssmenu" >
            <ul class="menucontainer">
                <li class="menuheader"><a class="menuheadertitle"><i class="fa fa-fw fa-home"></i> Chat</a></li>
                @foreach($users as $user)
                <li class="menuitem has-sub {{($user->online) ? 'online' : 'offline' }}" data-id="{{$user->_id}}" ><a class="menutitle" href="#"><i></i> {{$user->username}}</a>
                    <span class="online-status"></span>
                    <ul class="submenucontainer">
                        <li class="submenuitem">
                            <div class="chatcontainer">
                                <div class="panel panel-primary">
                                    <div class="panel-heading"><span class="glyphicon glyphicon-comment"></span> Chat</div>
                                    <div class="panel-body chat-body mainchat-body">
                                        <ul class="chat" id="{{$user->_id}}">
                                            {{-- Chat content goes here --}}
                                        </ul>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="input-group">
                                            <input id="chat-input-{{$user->_id}}" type="text" class="form-control input-sm" placeholder="Type your message here...">
                                            <input type="hidden" name="receiver_user_id" id="receiver_user_id" value="{{$user->_id}}">
                                            <span class="input-group-btn">
                                                <button class="btn btn-warning btn-sm btn-send-chat" id="btn-chat">Send</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </li>
                    </ul>
                </li>
                @endforeach
            </ul>
        </div> <!--End css menu -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Simple Sidebar</h1>
                        <p>This template has a responsive menu toggling system. The menu will appear collapsed on smaller screens, and will appear non-collapsed on larger screens. When toggled using the button below, the menu will appear/disappear. On small screens, the page content will be pushed off canvas.</p>
                        <p>Make sure to keep all page content within the <code>#page-content-wrapper</code>.</p>
                        <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css"  />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js"></script>  
        {!! Html::script('js/jquery.raty.js') !!}
    {!! Html::script('js/jquery-ui.min.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}
    {!! Html::script('js/underscore.min.js') !!}
    {!! Html::script('js/backbone.min.js') !!}
    {!! Html::script('js/adapter.js') !!}
    {!! Html::script('js/bootstrap-submenu.js') !!}

    {!! Html::script('js/script.js') !!}
    {!! Html::style('css/styles.css') !!}

    <!-- Menu Toggle Script -->
    <script>
        (function($) {
            $.fn.clickToggle = function(func1, func2) {
                var funcs = [func1, func2];
                this.data('toggleclicked', 0);
                this.on('click',function(event) {
                    var data = $(this).data();
                    var tc = data.toggleclicked;
                    $.proxy(funcs[tc], this)(event);
                    data.toggleclicked = (tc + 1) % 2;
                });
                return this;
            };
    
        }(jQuery));

    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $('.online-status').fadeToggle(200);
        $('.chatcontainer').fadeToggle(500);
        $("#wrapper").toggleClass("toggled");
    });
    $('[data-submenu]').submenupicker();
    $('.dropdown-toggle').dropdown()

  

    var activesubmenu;
    $( '.cssmenu > ul > li.menuitem' ).mouseenter( function(){
        $(this).addClass('active');
    } )
    .mouseleave( function(){
        if(!$(this).hasClass('current')){
            $(this).removeClass('active');
            
        }
    } );

    $('.cssmenu ul li.menuitem').clickToggle(function(event) { 
        if($(this).hasClass('has-sub')){
            console.log('occupied1A');
            $(this).children('ul').css('visibility', 'visible');  
            $( this).children('ul').addClass('active');
            $( this).addClass('active');
            $( this).addClass('current');

            if(activesubmenu){
                /* If the element clicked has a parent chat container dont remove chat */
                if(!$(event.target).parents('.chatcontainer').length){
                    console.log('occupied2A');
                    activesubmenu.children('ul').css('visibility', 'hidden');  
                    activesubmenu.children('ul').removeClass('active');
                    activesubmenu.removeClass('active');
                    activesubmenu.removeClass('current');
                    activesubmenu = null;
                }
            }
         
            activesubmenu = $(this);
        }else{
            if(activesubmenu){
                console.log('occupied3A');
                activesubmenu.children('ul').css('visibility', 'hidden');  
                activesubmenu.children('ul').removeClass('active');
                activesubmenu.removeClass('active');
                activesubmenu.removeClass('current');
                activesubmenu = null;
            }
        }

    },
    function(event) {
        if($(this).hasClass('has-sub')){
            if(!$(this).hasClass('current')){
                console.log('aquiB');
                $(this).children('ul').css('visibility', 'visible');  
                $( this).children('ul').addClass('active');
                $( this).addClass('active');
                $( this).addClass('current');

                if(activesubmenu){
                    console.log('occupied2B');
                    activesubmenu.children('ul').css('visibility', 'hidden');  
                    activesubmenu.children('ul').removeClass('active');
                    activesubmenu.removeClass('active');
                    activesubmenu.removeClass('current');
                    activesubmenu = null;
                }

                activesubmenu = $(this); 


            }else{
                console.log('aqui2B');
                /* If the element clicked has a parent chat container dont remove chat */
                if(!$(event.target).parents('.chatcontainer').length){

                    $(this).children('ul').css('visibility', 'hidden');  
                    $( this).children('ul').removeClass('active');
                    $( this).removeClass('active');
                    $( this).removeClass('current');   
                    activesubmenu = null;
                }
                activesubmenu = $(this); 
            }
        }else{
            if(activesubmenu){
                console.log('occupied3b');
                activesubmenu.children('ul').css('visibility', 'hidden');  
                activesubmenu.children('ul').removeClass('active');
                activesubmenu.removeClass('active');
                activesubmenu.removeClass('current');
                activesubmenu = null;
            }
        }
        // if($(this).hasClass('has-sub')){
        //     if(!$(this).hasClass('active')){
        //         $(this).children('ul').css('visibility', 'visible');  
        //         $( this).children('ul').addClass('active');
        //         $( this).addClass('active');
        //         $( this).addClass('current');
               
        //         activesubmenu = $(this);  
        //     }
        // }
        
    });


    // activesubmenu.clickToggle(function() { 
    //     console.log('here now');
    //     $(this).removeClass('subactive');
    // },
    // function() {
    // });

    </script>



</body>

</html>
