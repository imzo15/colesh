<!DOCTYPE html>
<html lang="en">
<head>

	@include('templates.partials.head')
	
</head>
<body>
	<div id="wrapper" style="margin-top:50px;">
		
		@include('templates.partials.navigation')
        @if(Auth::check())

		<div id="sidebar-wrapper" class="cssmenu" >
            <ul class="menucontainer">
                <li class="menuheader"><a class="menuheadertitle"><i class="fa fa-fw fa-home"></i> Chat</a></li>
                @foreach($chatusers as $user)
                <li class="menuitem has-sub {{($user->online) ? 'online' : 'offline' }}" data-id="{{$user->_id}}" ><a class="menutitle" href="#"><i></i> {{$user->username}}</a>
                    <span class="online-status"></span>
                    <ul class="submenucontainer">
                        <li class="submenuitem">
                            <div class="chatcontainer">
                                <div class="panel panel-primary">
                                    <div class="panel-heading"><span class="glyphicon glyphicon-comment"></span> Chat</div>
                                    <div class="panel-body chat-body mainchat-body">
                                        <ul class="chat" id="{{$user->_id}}">
                                            {{-- Chat content goes here --}}
                                        </ul>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="input-group">
                                            <input id="chat-input-{{$user->_id}}" type="text" class="form-control input-sm" placeholder="Type your message here...">
                                            <input type="hidden" name="receiver_user_id" id="receiver_user_id" value="{{$user->_id}}">
                                            <span class="input-group-btn">
                                                <button class="btn btn-warning btn-sm btn-send-chat" id="btn-chat">Send</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </li>
                    </ul>
                </li>
                @endforeach
            </ul>
        </div> <!--End css menu -->
		@endif
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
			@include('templates.partials.alerts')
			@yield('content')
					</div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->
	@include('templates.partials.footer')
</body>
</html>         