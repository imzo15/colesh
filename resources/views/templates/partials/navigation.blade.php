<nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}">COLESH</a>
                </div>
                @if(strpos(Request::url(), "/signin")==false) 
                    
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                    @if(Auth::check())
                        <form action="{{ route('search.results') }}" class="navbar-form navbar-left" role="form">
                            <div class="form-group">
                                <input type="text" name="query" class="form-control" placeholder="Find people">
                            </div>
                            <button class="btn btn-default" type="submit" >Search</button>
                        </form>    
                    @endif
                    @if(Auth::check())
                        <li class="active"><a href="">Inicio</a></li>
                        @if(Auth::user()->admin)
                            <li><a href="{{route('controlpanel')}}">Control Panel</a></li>
                        @endif
                        <li><a href="{{ route('profile.index', ['username' => Auth::user()->username ]) }}">{{ Auth::user()->getNameOrUsername() }}</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Configuraciones <span class="glyphicon glyphicon-cog"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="">Cambiar contraseña</a></li>
                                <li><a href="{{ route('profile.edit') }}">Editar Perfil</a></li>
                                <li><a href="" id="menu-toggle">Toggle Chat</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ route('auth.signout') }}">Cerrar Sesión</a></li>
                    </ul>
                    @else
                        
                        <form class="navbar-form navbar-right" method="POST" action="{{ route('auth.signin') }}">
                            <div class="form-group">
                                <input type="email" autofocus name="email" value="" placeholder="Email" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="password" placeholder="Password" name="password" id="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="remember"> <span style="color:white;">Recuérdame</span>
                            </div>
                            <button type="submit" class="btn btn-success">Ingresar</button>
                            {!! csrf_field() !!}
                        </form>
                    @endif
                </div><!--/.navbar-collapse -->
                @endif
            </div>
        </nav>