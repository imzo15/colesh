	<meta charset="UTF-8">
	<title>@yield('title')</title>


	<style type="text/css">

      /* Sticky footer styles
      -------------------------------------------------- */

      html,
      body {
        height: 100%;
        /* The html and body elements cannot have any padding or margin. */
      }

      /* Wrapper for page content to push down footer */
      #wrapper {
        min-height: 100%;
        height: auto !important;
        height: 100%;
        /* Negative indent footer by it's height */
        margin: 0 auto -30px;
      }

      /* Set the fixed height of the footer here */
      #push,
      #footer {
        height: 30px;
      }
      #footer {
        background-color: #f5f5f5;
      }

      /* Lastly, apply responsive CSS fixes as necessary */
      @media (max-width: 767px) {
        #footer {
          margin-left: -20px;
          margin-right: -20px;
          padding-left: 20px;
          padding-right: 20px;
        }
      }

    </style>

    {!! Html::script('js/jquery-2.2.0.min.js') !!}
    {!! Html::script('js/jquery.raty.js') !!}
    {!! Html::script('js/script.js') !!}
    {!! Html::script('js/jquery-ui.min.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}
    {!! Html::script('js/underscore.min.js') !!}
    {!! Html::script('js/backbone.min.js') !!}
    {!! Html::script('js/adapter.js') !!}
    {!! Html::script('js/bootstrap-submenu.js') !!}
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css"  />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js"></script>   

    {!! Html::style('css/bootstrap.min.css') !!}
    {!! Html::style('css/jquery-ui.min.css') !!}
    {!! Html::style('css/styles.css') !!}
    {!! Html::style('css/jquery.raty.css') !!}
    @if(strpos(Request::url(), "/calendar")==true)
    {!! Html::style('css/fullcalendar.min.css') !!}
    {!! Html::script('js/moment.min.js') !!}
    {!! Html::script('js/fullcalendar.min.js') !!}
    {!! Html::script('js/script.calendar.js') !!}
    <script src="http://cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>   
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css"  />
    @endif

    @if(strpos(Request::url(), "/controlpanel")==true)
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        #feedback { font-size: 1.4em; }
        #selectable .ui-selecting { background: #FECA40; }
        .selectable-group .ui-selected { background: #F39814; color: white; }
        .selectable-user .ui-selected { background: #3557DE; color: white; }
        #selectable { list-style-type: none; margin: 0; padding: 0; width: auto; }
        #selectable li { margin: 3px; padding: 1px; float: left; width: 100px; height: auto; font-size:1em; text-align: center; word-wrap: break-word; }
        #selectable:hover { cursor:pointer; }
    </style>
    @endif

    @if(strpos(Request::url(), "/kanban")==true)
    {!! Html::script('js/bootstrap-checkbox.js') !!}
    <style>
        #feedback { font-size: 1.4em; }
        #selectable .ui-selecting { background: #FECA40; }
        .selectable-group .ui-selected { background: #F39814; color: white; }
        .selectable-user .ui-selected { background: #3557DE; color: white; }
        #selectable { list-style-type: none; margin: 0; padding: 0; width: auto; }
        #selectable li { margin: 3px; padding: 1px; float: left; width: 100%; height: auto; font-size:1em; text-align: center; word-wrap: break-word; }
        #selectable:hover { cursor:pointer; }
    </style> 


    @endif
    {!! Html::script('js/adapter.js') !!}
    
    {!! Html::script('js/bootstrap-submenu.js') !!}
    
    