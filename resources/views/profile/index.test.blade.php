@extends('templates.default')
@section('title')
Hello
@stop
@section('content')
	<h3>Welcome Home</h3>
	<div class="row">
			<div class="col-lg-5">
				{{-- User information --}}
				@include('user.partials.userblock')
				<hr>

				@if(!$notes->count())
					<p>{{$user->getFirstNameOrUsername()}} hasn't posted anything yet.</p>
				@else
					@foreach($notes as $note)
						<div class="media">
							<a href="{{ route('profile.index', ['username' => $note->user->username]) }}" class="pull-left">
								<img src="{{ $note->user->getAvatarUrl() }}" alt=" {{ $note->user->getNameOrUsername() }} " class="media-object">
							</a>
							<div class="media-body">
								<h4 class="media-heading"><a href="{{ route('profile.index', ['username' => $note->user->username]) }}">{{ $note->user->getNameOrUsername() }}</a></h4>
								<p>{{ $note->note }}</p>
								<ul class="list-inline">
									<li>{{ $note->created_at->diffForHumans() }}</li>
									@if($note->user->_id !== Auth::user()->_id )
										<li><a href="{{ route('note.like', ['noteId' => $note->_id]) }}">Like</a></li>
									@endif
									<li> {{ $note->likes->count() }} {{ str_plural('like', $note->likes->count()) }}  </li>
								</ul>
								@foreach($note->replies as $reply)
									<div class="media">
										<a href="{{ route('profile.index', ['username' => $reply->user->username]) }}" class="pull-left">
											<img src="{{ $reply->user->getAvatarUrl() }}" alt="{{ $reply->user->getNameOrUsername() }}" class="media-object">
										</a>
										<div class="media-body">
											<h5 class="media-heading">
												<a href="{{ route('profile.index', ['username' => $reply->user->username]) }}">{{ $reply->user->getNameOrUsername() }}</a>
											</h5>
											<p> {{ $reply->note }} </p>
											<ul class="list-inline">
												<li>{{ $note->created_at->diffForHumans() }}</li>
												@if($reply->user->_id !== Auth::user()->_id )
													<li><a href="{{ route('note.like', ['noteId' => $reply->_id]) }}">Like</a></li>
												@endif
												<li> {{ $reply->likes->count() }} {{ str_plural('like', $reply->likes->count()) }}  </li>
											</ul>
										</div>
									</div>
								@endforeach
								@if($authUserIsFollowing || Auth::user()->_id === $note->user->_id)
									<form role="form" action="{{ route('note.reply', ['noteId' => $note->_id]) }}" method="post" >
										<div class="form-group {{ $errors->has("reply-{$note->_id}") ? 'has-error': '' }}">
											<textarea name="reply-{{$note->_id}}" id="" cols="30" rows="2" placeholder="Reply to this note" class="form-control"></textarea>
											@if($errors->has("reply-{$note->_id}"))
												<span class="help-block">{{ $errors->first("reply-{$note->_id}") }}</span>
											@endif
										</div>
										<input type="submit" value="Reply" class="btn btn-default btn-sm">
								    	{!! Form::token() !!}
									</form>
								@endif
							</div>
						</div>
					@endforeach
					<br><br><br>
					<br><br><br>
				@endif

			</div>
			<div class="col-lg-4 col-lg-offset-3">
				{{-- Friends, friend requests --}}
				{{--  --}}
				@if(Auth::user()->hasFollowRequestSentPendingToAccept($user) )
					<p>Waiting for {{ $user->getFirstNameOrUsername() }} to accept your request. </p>
				@elseif(Auth::user()->hasFollowRequestReceivedPending($user) )
					<a href=" {{ route('follow.accept', ['username' => $user->username]) }} " class="btn btn-primary">Accept follow request</a>
				@elseif(Auth::user()->isFollowing($user))
					<p>You are following {{ $user->getFirstNameOrUsername() }}. </p>
					<form action="{{route('follow.delete', ['username' => $user->username])}}" method="post">
						<input type="submit" value="Unfollow" class="btn btn-default">
				    	{!! Form::token() !!}
					</form>
				@elseif(Auth::user()->isFollowingMe($user))
					<p>You are being folowed by {{ $user->getFirstNameOrUsername() }}. </p>
				@else
					{{-- Change to and IF where you check if username is not the same --}}
					@if(Auth::user()->isMyProfile(Request::url()) )
						<a href="{{ route('follow.index') }}">Followers</a><br>
						<a href="{{ route('calendar.index') }}">Calendar</a>
					@else
						<a href=" {{ route('follow.add', ['username' => $user->username]) }} " class="btn btn-primary">Follow</a>
					@endif
				@endif
			
				<h4>{{ $user->getFirstNameOrUsername() }}'s following.</h4>
				@if(!$user->follow()->count())
					<p>You are not following anybody.</p>
				@else
					 @foreach( $user->follow() as $user )
					 	@include('user/partials/userblock')
					 @endforeach
				@endif
			</div>
		</div>	
@stop