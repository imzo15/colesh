@extends('templates.default')
@section('title')
Hello
@stop
@section('content')
	<h3>Welcome Home</h3>
	<div class="row">
		<div class="col-lg-3">
			{{-- User information --}}
			@include('user.partials.userblock')
			<hr>
			
			<!-- Nav tabs -->
			<ul class="nav nav-pills nav-stacked" role="tablist">
			    <li role="presentation" class="active"><a href="#activity" aria-controls="activity" role="tab" data-toggle="tab">My activity</a></li>
			    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
			    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
			    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
			  	</ul>

		</div>
		<div class="col-lg-9">

			
		  	<!-- Tab panes -->
		  	<div class="tab-content">
		    	<div role="tabpanel" class="tab-pane active" id="activity">
				  	<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#myquestions" aria-controls="myquestions" role="tab" data-toggle="tab">My Questions</a></li>
					    <li role="presentation"><a href="#myanswers" aria-controls="myanswers" role="tab" data-toggle="tab">My answers</a></li>
					    <li role="presentation"><a href="#questionsforme" aria-controls="questionsforme" role="tab" data-toggle="tab">Questions for Me</a></li>
					    <li role="presentation"><a href="#answersforme" aria-controls="answersforme" role="tab" data-toggle="tab">Answers for Me</a></li>
					 </ul>

					<!-- Tab panes -->
					<div class="tab-content">
					    <div role="tabpanel" class="tab-pane active" id="myquestions">
					    	@foreach($myQuestions as $question)
					    		<div class="panel panel-default" >
									<a href="{{ route('question.index', ['id' => $question->_id]) }}" class="question-panel">
							  		<div class="panel-body question-results-panel-body">
										<h3 class="question-results-title">{{$question->title}}</h3>			
											
										<ul class="list-inline">
											<li>{{$question->description}}</li>
											<hr class="question-results-hr">
											<li>Created: {{ $question->created_at->diffForHumans() }}</li> by {{ $question->user->getNameOrUsername() }}
											<span class="label label-default">
												{{ $question->answers->count() }} {{ str_plural('answer', $question->answers->count()) }}
											</span>
										</ul>
							  		</div>
									</a>	
							  		@if(count($question->images))
							  		<div class="panel-footer">
										@foreach($question->images as $image)
											<div class="question-image-thumb">
												<img src="/{{$image->path}}/thumb_{{$image->name}}.{{$image->extension}}" alt="{{$image->title}}" data-toggle="modal" data-target="#questionImageModal" data-alttitle="{{$question->title}}" data-title="{{$image->title}}" data-path="{{$image->path}}" data-name="{{$image->name}}" data-extension="{{$image->extension}}">
											</div>
										@endforeach
							  		</div>
							  		@endif
								</div>
					    	@endforeach
					    </div>
					    <div role="tabpanel" class="tab-pane" id="myanswers">
					    	@foreach($myAnswers as $answer)
								<div>
									{{$answer->question->title}}
									<br>
									{{$answer->answer}}
									{{$answer->user->getNameOrUsername()}}
									<hr>
								</div>
							@endforeach
					    </div>
					    <div role="tabpanel" class="tab-pane" id="questionsforme">..asd.</div>
					    <div role="tabpanel" class="tab-pane" id="answersforme">..aaqw.</div>
					</div>

		    	</div>
		    	<div role="tabpanel" class="tab-pane" id="profile">.2..</div>
		    	<div role="tabpanel" class="tab-pane" id="messages">3...</div>
		    	<div role="tabpanel" class="tab-pane" id="settings">...4</div>
		 	</div>
		</div>
	</div>	
@stop