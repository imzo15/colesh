@extends('templates.default')
@section('title')
Sign Up
@stop
@section('content')
	<div style="margin-top:30px;"></div>
	<div class="row">
		<div class="col-lg-3"></div>
			<div class="col-lg-6">
	    	<h2 class="form-signin-heading">Sign up</h2>
			<form action="{{ route('auth.signup') }}" class="form-vertical" method="post" role="form">
				<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} ">
					<label for="email" class="control-label">Email</label>
					<input type="text" name="email" class="form-control" id="email" value="{{ Request::old('email') ? : '' }}" autofocus>
					@if($errors->has('email'))
						<span class="help-block">{{ $errors->first('email') }}</span>
					@endif
				</div>
				<div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
					<label for="username" class="control-label">Username</label>
					<input type="text" name="username" class="form-control" id="username" value="{{ Request::old('password') ? : '' }}">
					@if($errors->has('username'))
						<span class="help-block">{{ $errors->first('username') }}</span>
					@endif
				</div>
				<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
					<label for="password" class="control-label">Password</label>
					<input type="password" name="password" class="form-control" id="password">
					@if($errors->has('password'))
						<span class="help-block">{{ $errors->first('password') }}</span>
					@endif
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-default">Sign Up</button>
				</div>
				{!! Form::token() !!}
					
			</form>
		</div>
		<div class="col-lg-3"></div>
	</div>
	
<!-- 
	<form class="form-signin">
    	<h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <div class="checkbox">
          	<label>
            	<input type="checkbox" value="remember-me"> Remember me
         	 </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
 -->
@stop