@extends('templates.default')
@section('title')
Sign In
@stop
@section('content')
	<div style="margin-top:30px;"></div>
	<div class="row">
		<div class="col-lg-3"></div>
			<div class="col-lg-6">
			<form action="{{ route('auth.signin') }}" class="form-signin" method="post" role="form">
		    	<h2 class="form-signin-heading">Sign in</h2>
		    	<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} ">
					<label for="email" class="control-label">Email</label>
					<input type="text" name="email" class="form-control" id="email" value="{{ Request::old('email') ? : '' }}" placeholder="Email" autofocus>
					@if($errors->has('email'))
						<span class="help-block">{{ $errors->first('email') }}</span>
					@endif
				</div>
				<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
					<label for="password" class="control-label">Password</label>
					<input type="password" name="password" class="form-control" id="password" placeholder="Password">
					@if($errors->has('password'))
						<span class="help-block">{{ $errors->first('password') }}</span>
					@endif
				</div>

		        <div class="checkbox">
		          	<label>
		            	<input type="checkbox" name="remember"> Remember me
		         	</label>
		        </div>
		        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
		    	{!! Form::token() !!}
					
			</form>
		</div>
		<div class="col-lg-3"></div>
	</div>
	

@stop