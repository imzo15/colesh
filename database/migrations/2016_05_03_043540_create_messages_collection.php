<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesCollection extends Migration
{
    /**
     * Run the migrations.
     * increments id
     * objectid receiver_user_id
     * objectid sender_user_id
     * string message
     * bool read
     * timestamps
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function (Blueprint $collection) {
            //
        });
    }
}
