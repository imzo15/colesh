<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelsSpecialitiesUsersCollection extends Migration
{
    /**
     * Run the migrations.
     * 
     * increments id
     * objectid user_id
     * objectid speciality_id
     * objectid level_id
     * int points
     * timestamps
     *
     * @return void
     */
    public function up()
    {
        Schema::table('levels_specialities_users', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('levels_specialities_users', function (Blueprint $collection) {
            //
        });
    }
}
