<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsCollection extends Migration
{
    /**
     * Run the migrations.
     * increments id
     * objectid user_id
     * string name
     * string description
     * timestamps
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups', function (Blueprint $collection) {
            //
        });
    }
}
