<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialitiesUsersCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * increments id
     * objectid user_id
     * objectid speciality_id
     * string speciality_name
     * bool activated
     *
     * @return void
     */
    public function up()
    {
        Schema::table('specialities_users', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('specialities_users', function (Blueprint $collection) {
            //
        });
    }
}
