<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKanbancolumnsCollection extends Migration
{
    /**
     * Run the migrations.
     * 
     * increments id
     * objectid kanbanboard_id
     * string name
     * string comments
     * string color
     * int max
     * timestamps
     * @return void
     */
    public function up()
    {
        Schema::table('kanbancolumns', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kanbancolumns', function (Blueprint $collection) {
            //
        });
    }
}
