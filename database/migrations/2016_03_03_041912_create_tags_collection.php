<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsCollection extends Migration
{
    /**
     * Run the migrations.
     * increments id
     * string name
     * timestamps
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tags', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tags', function (Blueprint $collection) {
            //
        });
    }
}
