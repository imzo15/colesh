<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * increments id
     * objectid model_id
     * string type
     * string title
     * string path
     * string name
     * string extension
     * 
     * @return void
     */
    public function up()
    {
        Schema::table('images', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images', function (Blueprint $collection) {
            //
        });
    }
}
