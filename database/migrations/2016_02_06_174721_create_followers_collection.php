<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowersCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * increments id
     * objectid follower_id
     * objectid followed_id
     * bool accepted
     * timestamps
     * 
     * @return void
     */
    public function up()
    {
        Schema::table('followers', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('followers', function (Blueprint $collection) {
            //
        });
    }
}
