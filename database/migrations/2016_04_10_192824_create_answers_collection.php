<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersCollection extends Migration
{
    /**
     * Run the migrations.
     * increments id
     * objectid user_id
     * objectid question_id
     * string answer
     * int likes
     * timestamps
     * @return void
     */
    public function up()
    {
        Schema::table('answers', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answers', function (Blueprint $collection) {
            //
        });
    }
}
