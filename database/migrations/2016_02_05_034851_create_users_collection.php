<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersCollection extends Migration
{
    /**
     * Run the migrations.
     * 
     * increments id
     * string email
     * string username
     * string password
     * string first_name null
     * string last_name null
     * string location null
     * bool online
     * string remember_token null
     * timestamps
     * 
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $collection) {
            //
        });
    }
}
