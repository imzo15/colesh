<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSyncTasksCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * increments id
     * string sync_id
     * objectid task_id
     * objectid user_task_id
     * objectid last_edit_user_id
     * 
     * 
     * @return void
     */
    public function up()
    {
        Schema::table('synctasks', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('synctasks', function (Blueprint $collection) {
            //
        });
    }
}
