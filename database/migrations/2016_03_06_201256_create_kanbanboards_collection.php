<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKanbanboardsCollection extends Migration
{
    /**
     * Run the migrations.
     * increments id
     * objectid user_id
     * string name
     * string comments
     * timestamps
     * 
     * @return void
     */
    public function up()
    {
        Schema::table('kanbanboards', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kanbanboards', function (Blueprint $collection) {
            //
        });
    }
}
