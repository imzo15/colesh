<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsCollection extends Migration
{
    /**
     * Run the migrations.
     * increments id
     * objectid user_id
     * objectid model_id
     * string type
     * int rating
     * timestamps
     * @return void
     */
    public function up()
    {
        Schema::table('ratings', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ratings', function (Blueprint $collection) {
            //
        });
    }
}
