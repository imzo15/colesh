<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuperiorsSubordinatesCollection extends Migration
{
    /**
     * Run the migrations.
     * increments id
     * objectid superior_id
     * string superior_username
     * objectid subordinate_id
     * string subordinate_username
     *
     * @return void
     */
    public function up()
    {
        Schema::table('superiors_subordinates', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('superiors_subordinates', function (Blueprint $collection) {
            //
        });
    }
}
