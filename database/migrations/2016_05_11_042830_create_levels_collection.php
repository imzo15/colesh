<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelsCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * increments id
     * string name
     * objectid speciality_id
     * int level
     * string image_path
     * int points_required
     * timestamps
     * @return void
     */
    public function up()
    {
        Schema::table('levels', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('levels', function (Blueprint $collection) {
            //
        });
    }
}
