<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * increments id
     * objectid user_id
     * string title
     * string description
     * 
     * @return void
     */
    public function up()
    {
        Schema::table('questions', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function (Blueprint $collection) {
            //
        });
    }
}
