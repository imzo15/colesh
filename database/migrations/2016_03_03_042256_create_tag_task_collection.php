<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagTaskCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * increments id
     * objectid task_id
     * objectid tag_id
     * timestamps
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tag_task', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tag_task', function (Blueprint $collection) {
            //
        });
    }
}
