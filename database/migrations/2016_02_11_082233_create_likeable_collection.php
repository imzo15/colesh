<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLikeableCollection extends Migration
{
    /**
     * Run the migrations.
     * increments id
     * objectid user_id
     * objectid likeable_id
     * string likeable_type
     * timestamps
     *
     * @return void
     */
    public function up()
    {
        Schema::table('likeable', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('likeable', function (Blueprint $collection) {
            //
        });
    }
}
