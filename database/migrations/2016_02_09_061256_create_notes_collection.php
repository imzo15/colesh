<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesCollection extends Migration
{
    /**
     * Run the migrations.
     * increments id
     * objectid user_id
     * string title
     * string note
     * int likes
     * timestamps
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notes', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notes', function (Blueprint $collection) {
            //
        });
    }
}
