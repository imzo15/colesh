<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialitiesCollection extends Migration
{
    /**
     * Run the migrations.
     * increments id
     * string name
     * timestamps
     * @return void
     */
    public function up()
    {
        Schema::table('specialities', function (Blueprint $collection) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('specialities', function (Blueprint $collection) {
            //
        });
    }
}
