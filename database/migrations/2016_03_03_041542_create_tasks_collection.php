<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * increments id
     * objectid user_id
     * string user_username
     * objectid user_creator_id
     * objectid sync_id
     * string name
     * string description
     * string color
     * string comment
     * bool editable
     * string start_date
     * string due_date
     * string done_date
     * bool pending
     * bool realesed
     * timestamp realesed_at
     * string column_index
     * objectid column_id
     * objectid kanban_id
     * timestamps
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $collection) {
            //
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $collection) {
            //
        });
    }
}
