var printout = function(text){
    console.log(text);
};

    
$(function() {
    
    /* Gets ids of chat users */
    $( ".menucontainer" ).each(function( index ) {
        if ($(this).children().hasClass('menuitem')){
            var elem = $(this).children('.has-sub');
            for (var i = 0; i < elem.length; i++) {
                console.log(elem[i].dataset.id);
            };
        }
    });

    /*Comment*/

    /* Initialize toggled chat */
    $('#wrapper').attr('class', 'toggled');
    $('.online-status').hide();


    /* Fadeout the alerts with errors (global-danger & global-success) */
    setTimeout(function(){

        $('.fade-out').fadeOut( 1000, function() {
            $( this ).remove();
        });;
    }, 5000);

    $.ajaxSetup({   
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')                   
        }   
    });
    $(".panel-users").hide();
    $(".panel-users-group").hide();
    $(document).on("click", ".selectable-group > li", function(event){
        $(".panel-users").show();
        $(".panel-users-group").show();
        //Remove first and last space
        thisGroup = $(this).html().slice(1, -1);
        printout('h' +thisGroup + 'h');
    });
        
/*
    $( ".selectable-group" ).selectable({
        cancel: "li,.cancel"
    });*/
    var groupname;
    $(".selectable-group > li").on("click", function(event){
        $('.selectable-group > li').removeClass('ui-selected');
        $(this).addClass('ui-selected');
        //Remove first and last space
        groupname = $(this).html().slice(1, -1);
        $(".panel-users-group > .panel-heading").text("Users in " + groupname);
        var data = {
            _token:$(this).data('token'),
            groupname: groupname,
        }
        $.post('/groupuser/get', data, function(data){
            //console.log(data);
            $( ".panel-body-users-group" ).html("");    
            var token = $('meta[name="csrf-token"]').attr('content');
            var items = [];
              $.each( data, function( key, val ) {
                items.push( "<li class='ui-state-default' data-token='" + token +"''>" + val.username + "</li>" );
              });
             
              $( "<ol/>", {
                "id": "selectable",
                "class": "ui-selectable selectable-user-group",
                html: items.join( "" )
              }).appendTo( ".panel-body-users-group" );
        });

    });

    $( ".selectable-user" ).selectable({
        cancel: "li,.cancel"
    });

    /* Add user to group */
    $(document).on("click", ".selectable-user > li", function(event){
        var username = $(this).html().replace(/\s/g, '');
        console.log(username + " group: " + groupname +".");
        $(".panel-users-group .selectable-user-group").append($(this));
        var data = {
            _token:$(this).data('token'),
            groupname: groupname,
            username: username,
        }

        $.post('/groupuser/add', data, function(data){
            //console.log(data);    
        });
        
    });


    /* Remove user from group */
    $(document).on("click", ".selectable-user-group > li", function(event){
        var username = $(this).html().replace(/\s/g, '');
        console.log(username + "group");
        $(".panel-users .selectable-user").append($(this));
        var group = $(".selectable-group > .ui-selected").html();
        printout(group);
        var data = {
            _token:$(this).data('token'),
            groupname: groupname,
            username: username,
        }
        $.post('/groupuser/remove', data, function(data){
            //console.log(data);    
        });
    
    });


    $( ".datepicker" ).datepicker();
    $( ".datepicker" ).datepicker('setDate', new Date());

    $('#tag_list').select2({
        placeholder : 'Choose a tag',
    });

    $('#user_list').select2({
        placeholder : 'Choose a user',
    });

    /* Dropdown menu */
    $(".dropdown-item").bind('click', function(){
        var value = $(this).children('a').text();
        //var colorkey = $(this).children('span').text();
        $(".input-hiddem-dropdown-item").val(value);
        $(this).parent().prev().children(".dropdown-menu-text").text(value);
    });


    $( ".column-kanban" ).disableSelection();

    // $('#editabletask-checkbox').checkboxpicker({
    //     html: true,
    //     offLabel: '<span class="glyphicon glyphicon-remove">',
    //     onLabel: '<span class="glyphicon glyphicon-ok">'
    // });

/* ************  Make tasks draggables and columns dropables and sortables ************ */

    $('.panel-body-pending').sortable({
        connectWith: '.panel-body-todo, .panel-body-today, .panel-body-inprogress, .panel-body-done',
        receive: function(event, ui){
            var id = $(ui.item).data('id');
            var column_name = 'Pending';
            var params = {id: id, column_name: column_name};
            $.get('/kanbantask/changeColumn', params, function(data){
                console.log(data);  
            });
        },
    });

    $('.panel-body-todo').sortable({
        connectWith: '.panel-body-pending, .panel-body-today, .panel-body-inprogress, .panel-body-done',
        receive: function(event, ui){
            var id = $(ui.item).data('id');
            var column_name = 'To-Do';
            var params = {id: id, column_name: column_name};
            $.get('/kanbantask/changeColumn', params, function(data){
                console.log(data);  
            });
        },
    });

    $('.panel-body-today').sortable({
        connectWith: '.panel-body-pending, .panel-body-todo, .panel-body-inprogress, .panel-body-done',
        receive: function(event, ui){
            var id = $(ui.item).data('id');
            var column_name = 'Today';
            var params = {id: id, column_name: column_name};
            $.get('/kanbantask/changeColumn', params, function(data){
                console.log(data);  
            });
        },
    });

    $('.panel-body-inprogress').sortable({
        connectWith: '.panel-body-pending, .panel-body-todo, .panel-body-today, .panel-body-done',
        receive: function(event, ui){
            var id = $(ui.item).data('id');
            var column_name = 'In-progress';
            var params = {id: id, column_name: column_name};
            $.get('/kanbantask/changeColumn', params, function(data){
                console.log(data);  
            });
        },
    });

    $('.panel-body-done').sortable({
        connectWith: '.panel-body-pending, .panel-body-todo, .panel-body-today, .panel-body-inprogress',
        receive: function (event, ui) {
            /*if ($(ui.item).hasClass('special')) {
                ui.sender.sortable('cancel');
            } */
            var id = $(ui.item).data('id');
            var column_name = 'Done';
            var params = {id: id, column_name: column_name};
            $.get('/kanbantask/changeColumn', params, function(data){
                console.log(data);  
            });
            $('#taskModal').modal({ show: false}) 
            $('#taskModal').modal('show');    

        },
    });


/* Scroll Efect for headers of tasks in kanban board */
$('.scrollEffectText').bind({mouseenter: function() {
        var container = $(this);
        container.css('text-overflow', 'clip');
        var content = container.children('span.overflowContent');
        var offset = container.width() - content.width();
        if (offset < 0) {
            content.animate({
                'margin-left': offset
            }, {
                duration: offset * -10, 
                easing: 'linear'
            });
        } 
    },
    mouseleave: function(){
        //controller.scrollingText.reset(this);
        var containter = $(this);
        containter.css('text-overflow', 'ellipsis');
        var content = containter.children('span.overflowContent');
        var offset = containter.width() - content.width();
        if (offset < 0) {
            content.clearQueue().stop();
            content.css('margin-left', 0);
        }
        
    }
    });

/* ************ Modal for task ************ */
    $('#taskModal').on('show.bs.modal', function (event) {
        var task = $(event.relatedTarget);
        var id = task.data('id');
        var due_date = task.data('due_date');
        var color = task.data('color');
        var name = task.children('.panel-heading-task').children('span')[0].innerHTML;
        var description = task.children('.panel-body')[0].innerHTML;
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modalHeader = $('.modal-header').children('h4');
        var modalBodyElements = $('.modal-body').children('form');

        modalBodyElements.children('input[type=hidden]').val(id);
        modalHeader.text(name);
        modalBodyElements.children('.form-group').children('#description').text(description);

        var colors ={Red : '#FF4D4D', Blue : '#2332FF', Green : '#1DD613', Yellow : '#F1F734', Gray : '#858585', Purple : '#BA23FF', Pink : '#FF23F0', Orange : '#FFAE23', 'No Color' : '#F5F5F5'};
        var coloroutput;
        $.each( colors, function( key, value ) {
            if (value==color)
                coloroutput = key;
        });
        modalBodyElements.children('.form-group').children('.btn-group').children('button').children('#color-btn-text').text(coloroutput);
        modalBodyElements.children('.btn-group').children('div').children('input').datepicker( "setDate", due_date );
    });



    
    $("#task-modal-submit").bind('click', function(e){
        e.preventDefault();
        var modalHeader = $('.modal-header').children('h4');
        var modalBodyElements = $('.modal-body').children('form');
        var id = modalBodyElements.children('input[type=hidden]').val();
        var name = modalHeader.text();
        var description = modalBodyElements.children('.form-group').children('#description').val();
        var color = modalBodyElements.children('.form-group').children('.btn-group').children('button').children('#color-btn-text').text();
        var due_date = modalBodyElements.children('.form-group').children('.btn-group').children('input').val();
        
        var params = {id: id, due_date: due_date, color: color, name: name, description: description};
        $.post('/kanbantask/edit', params, function(data){
            console.log(data);  
        });          
        $('#taskModal').modal('hide');    
    });


    /* Results page images */
    /* ************ Modal for image question ************ */
    $('#questionImageModal').on('show.bs.modal', function (event) {
        var questionImage = $(event.relatedTarget);
        var title = questionImage.data('title');
        var alttitle = questionImage.data('alttitle');
        var path = questionImage.data('path');
        var name = questionImage.data('name');
        var extension = questionImage.data('extension');

        var modalHeader = $('.modal-header').children('h4');
        var modalBodyElements = $('.modal-body');
        if(title=="")
            modalHeader.text(alttitle);
        else
            modalHeader.text(title);
        modalBodyElements.children('#questionImageimg').attr('src','/'+path+'/'+name+'.'+extension);

    });


    /* Question raty ajax request */
    $('.rate-answer').raty({
        click: function(score, evt) {
            $(this).raty('readOnly',true);
            var id = $(this).data("id");
            printout(score);
            var data = {
                _token:$(this).data('token'),
                id:id,
                score: score,
            }
            $.get('/answer/rate', data, function(data){
                console.log(data);   
                $(this).attr('data-score', score); 
                $(this).attr('ro', "true"); 
                printout('here' + $(this).attr('data-score'));
                printout('here' + $(this).attr('ro'));
            }); 
        },
        half: function() {
            return true;
        },
        halfShow: function() {
            return true;
        },
        score: function() {
            return $(this).attr('data-score');
        },
        readOnly: function() {
            return $(this).attr('ro');
        }
    });

    $(".rate-answer").bind('rated', function (event, value) { 
        alert('You\'ve rated it: ' + value); 
    });

    /**************************************** CONTROL PANEL ***************************************/
    
    /* ******************************** Manage superiors ******************************** */
    $('.panel-subordinates-available').hide();
    $('.panel-subordinates').hide();
    $('.list-group-item-superior').on('click', function(event){
        $('.panel-subordinates-available').show();
        $('.panel-subordinates').show();
    });

        /* Add subordinate to user */
    $(".panel-body-subordinates-available").on("click", ".selectable-subordinates-available > li", function(event){
        var subordinate_id = $(this).data('us')
        $(".selectable-subordinates").append($(this));
        var us_id = $('.selectable-subordinates-available').data('us');
        printout('asdasd ' + us_id);
        var data = {
            _token:$(this).data('token'),
            superior_id: us_id,
            subordinate_id: subordinate_id,
        }
        printout(data);
        // 56b5aacc25c18365358b4569
        $.post('/superiorsubordinate/add', data, function(data){
            console.log(data);    
        });
        
    });

    /* Remove subordinate from user */
    $(".panel-body-subordinates").on("click", ".selectable-subordinates > li", function(event){
        var subordinate_id = $(this).data('us')
        $(".selectable-subordinates-available").append($(this));
        var us_id = $('.selectable-subordinates-available').data('us');
        printout('remove ' + us_id);
        var data = {
            _token:$(this).data('token'),
            superior_id: us_id,
            subordinate_id: subordinate_id,
        }
        printout(data);
        $.post('/superiorsubordinate/remove', data, function(data){
            console.log(data);    
        });
        
    });

    $('.list-group-item-superior').on('click', function(event){
        $('.list-group > .list-group-item-superior').removeClass('selected-list-item');
        $(this).addClass('selected-list-item');
        var user_id = $(this).data('us')
        console.log(user_id);

        var data = {
            _token:$(this).data('token'),
            user_id: user_id,
        }
        var token = $('meta[name="csrf-token"]').attr('content');
        /*Get subordinates available for user*/
        $.post('/subordinatesavailable/get', data, function(data){
            console.log('available');
            console.log(data);
            $( ".panel-body-subordinates-available" ).html("");    
            var items = [];
              $.each( data, function( key, val ) {
                items.push( "<li class='ui-state-default' data-token='" + token +"' data-us='" + val._id +"'>" + val.username + "</li>" );
              });
              $( "<ol/>", {
                "id": "selectable",
                "class": "ui-selectable selectable-subordinates-available",
                html: items.join( "" )
              }).data('us', user_id).appendTo( ".panel-body-subordinates-available" );
        });
        /* Get subordinates from user */
        $.post('/superiorsubordinate/get', data, function(data){
            console.log(data);
            $( ".panel-body-subordinates" ).html("");    
            var items = [];
              $.each( data, function( key, val ) {
                items.push( "<li class='ui-state-default' data-token='" + token +"' data-us='" + val.subordinate_id +"'>" + val.subordinate_username + "</li>" );
              });
              $( "<ol/>", {
                "id": "selectable",
                "class": "ui-selectable selectable-subordinates",
                html: items.join( "" )
              }).data('us', user_id).appendTo( ".panel-body-subordinates" );
        });
    });


    /* ***************************** Manage speciality levels ***************************** */
    $('.create-level-container').hide();
    
    $('.list-group-item-speciality').on('click', function(event){
        $('.create-level-container').show();
        event.preventDefault();
        $('.list-group > .list-group-item-speciality').removeClass('selected-list-item');
        $(this).addClass('selected-list-item');
        var speciality_id = $(this).data('sp');
        console.log(speciality_id);
        $('#level_speciality_id').val(speciality_id);
        var data = {
            _token:$(this).data('token'),
            speciality_id: speciality_id,
        }

        var token = $('meta[name="csrf-token"]').attr('content');
        $.post('/level/get', data, function(data){
            console.log(data);
            $( ".tbody-level" ).html("");    
            var items = [];

              $.each( data, function( key, val ) {
                val.name
                val.points_required
                val.level
                val.speciality_id
                items.push( "<tr><th scope='row'>"+val.level+"</th><td>"+val.name+"</td><td>image_path</td><td>"+val.points_required+"</td></tr>" );
              });
              //$( "<ads/>", {
                //html: items.join( "" )
            $( ".tbody-level" ).html(items.join( "" ));    
              //}).data('sp', speciality_id).appendTo( ".tbody-level" );
        });
        
    });

    $(".dropdown-post-section-item").mouseenter( function(){
        $(this).css('background-color', 'red');
    } )
    .mouseleave( function(){
        $(this).css('background-color', 'white');
    } );
    // $(".dropdown-post-section-item").hover(function(){
    //     $(this).css('background-color', 'red');
    //     printout('in');
    // }, function(){
    //     $(this).css('background-color', '#F5F5F5');
    //     printout('out');
    // });

    /* ***************************** Manage users tasks ***************************** */
    $('#kanbantasks-tab').on('click', function(){
        printout('hello');
        $( ".taskscontent" ).html("");    
        var data = {};
        $.post('/managetasks/get', data, function(data){
            console.log(data);   
            var items = [];
            var current_username = "";
            $.each( data, function( key, val ) {
                if (current_username != val.user_username){
                    items.push("<div class='col-md-12 text-center'><b>"+val.user_username+"</b></div>");
                    items.push("<div class='col-md-2 col-pending' id='col-pending-task-"+val.user_id+"'></div>");
                    items.push("<div class='col-md-3 col-todo' id='col-todo-task-"+val.user_id+"'></div>");
                    items.push("<div class='col-md-3 col-today' id='col-today-task-"+val.user_id+"'></div>");
                    items.push("<div class='col-md-2 col-inprogress' id='col-inprogress-task-"+val.user_id+"'></div>");
                    items.push("<div class='col-md-2 col-done' id='col-done-task-"+val.user_id+"'></div>");

                }
                current_username = val.user_username;
            });
            //$( "<ads/>", {
            //html: items.join( "" )
            $( ".taskscontent" ).html(items.join( "" ));  


            $.each( data, function( key, val ) {
                var html = "<div data-sp='" +val._id+"' >"+val.name + "</div>";
                if (val.column_index == 0){
                    $('#col-pending-task-' + val.user_id+'').append(html);
                }
                if (val.column_index == 1){
                    $('#col-todo-task-' + val.user_id+'').append(html);
                }
                if (val.column_index == 2){
                    $('#col-today-task-' + val.user_id+'').append(html);
                }
                if (val.column_index == 3){
                    $('#col-inprogress-task-' + val.user_id+'').append(html);
                }
                if (val.column_index == 4){
                    html = "<div data-toggle='modal' data-target='#doneTaskModal' data-sp='" +val._id+"' >"+val.name + "</div><span style='display:none;'>"+val.description+"</span>";
                    $('#col-done-task-' + val.user_id+'').append(html);
                }
                current_username = val.user_username;
            });
        });
    });

    $("#task-modal-realese").bind('click', function(e){
        e.preventDefault();
        var modalBodyElements = $('.modal-body').children('form');
        var id = modalBodyElements.children('input[type=hidden]').val();
        var params = {id: id};
        $.post('/kanbantask/realese', params, function(data){
            console.log(data);  
        });          
        $('#doneTaskModal').modal('hide');    
    });

    /* Modal for done user task */
    $('#doneTaskModal').on('show.bs.modal', function (event) {
        var task = $(event.relatedTarget);
        printout(task);
        var id = task.data('sp');
        var name = task[0].innerHTML;
        var description = task[0].nextSibling.innerHTML;
        var modalHeader = $('.modal-header').children('h4');
        var modalBodyElements = $('.modal-body').children('form');
        modalBodyElements.children('input[type=hidden]').val(id);

        modalHeader.text(name);
        modalBodyElements.children('.form-group').children('#description').text(description);

    });

    /* ***************************** Manage user specialities ***************************** */
    $('.panel-specialities-available').hide();
    $('.panel-specialities-user').hide();
    $('.list-group-item-user').on('click', function(event){
        $('.panel-specialities-available').show();
        $('.panel-specialities-user').show();
    });

    /* Add speciality to user */
    $(".panel-body-specialities-available").on("click", ".selectable-specialities-available > li", function(event){
        var sp_id = $(this).data('sp')
        $(".selectable-specialities-user").append($(this));
        var us_id = $('.selectable-specialities-available').data('us');
        var data = {
            _token:$(this).data('token'),
            user_id: us_id,
            speciality_id: sp_id,
        }
        printout(data);
        // 56b5aacc25c18365358b4569
        $.post('/specialityuser/add', data, function(data){
            console.log(data);    
        });
        
    });

    /* Remove speciality from user */
    $(".panel-body-specialities-user").on("click", ".selectable-specialities-user > li", function(event){
        var sp_id = $(this).data('sp')
        $(".selectable-specialities-available").append($(this));
        var us_id = $('.selectable-specialities-user').data('us');
        printout('asdasd ' + us_id);
        var data = {
            _token:$(this).data('token'),
            user_id: us_id,
            speciality_id: sp_id,
        }
        printout(data);
        $.post('/specialityuser/remove', data, function(data){
            console.log(data);    
        });
        
    });

    $('.list-group-item-user').on('click', function(event){
        $('.list-group > .list-group-item-user').removeClass('selected-list-item');
        $(this).addClass('selected-list-item');
        var user_id = $(this).data('us')
        console.log(user_id);

        var data = {
            _token:$(this).data('token'),
            user_id: user_id,
        }
        var token = $('meta[name="csrf-token"]').attr('content');
        /*Get specialities available for user*/
        $.post('/specialityavailableuser/get', data, function(data){
            console.log('available');
            console.log(data);
            $( ".panel-body-specialities-available" ).html("");    
            var items = [];
              $.each( data, function( key, val ) {
                items.push( "<li class='ui-state-default' data-token='" + token +"' data-sp='" + val._id +"'>" + val.name + "</li>" );
              });
              $( "<ol/>", {
                "id": "selectable",
                "class": "ui-selectable selectable-specialities-available",
                html: items.join( "" )
              }).data('us', user_id).appendTo( ".panel-body-specialities-available" );
        });

        /*Get specialities the user has*/
        $.post('/specialityuser/get', data, function(data){
            console.log(data);
            $( ".panel-body-specialities-user" ).html("");    
            var items = [];
              $.each( data, function( key, val ) {
                items.push( "<li class='ui-state-default' data-token='" + token +"' data-sp='" + val.speciality_id +"'>" + val.speciality_name + "</li>" );
              });
              $( "<ol/>", {
                "id": "selectable",
                "class": "ui-selectable selectable-specialities-user",
                html: items.join( "" )
              }).data('us', user_id).appendTo( ".panel-body-specialities-user" );
        });

     });

    /**************************************** CHAT SIDEBAR ****************************************/
    /**********************************************************************************************/

    var lastTimeID = 0;

     $('#btnSend').click( function() {
        sendChatText();
        $('#chatInput').val("");
    });

     $('.btn-send-chat').bind('click', function(event){
        var id = $(this).parent('.input-group-btn').siblings(':hidden').val(); 
        sendChatText(id);
     });

     $('.menuitem').bind('click', function(event){
        
        var id = $(this).data('id');
        if(id !== undefined){
            startChat(id, event);
        }
     });
    //startChat();





/*    $("#search-content-submit").bind('click', function(e){
        e.preventDefault();
        var search = $('#content-search').val();
        
        var params = {search: search};
        printout(params);
        $.get('/search/content', params, function(data){
            console.log(data);  
        });          
            
    });*/

//     var servers = null;
//     var pc = new webkitRTCPeerConnection(servers,
//       {optional: [{RtpDataChannels: true}]});

//     pc.ondatachannel = function(event) {
//       receiveChannel = event.channel;
//       receiveChannel.onmessage = function(event){
//         document.querySelector("div#receive").innerHTML = event.data;
//       };
//     };

//     sendChannel = pc.createDataChannel("sendDataChannel", {reliable: false});

//     document.querySelector("button#send").onclick = function (){
//       var data = document.querySelector("textarea#send").value;
//       sendChannel.send(data);
// };
    



            $.fn.clickToggle = function(func1, func2) {
                var funcs = [func1, func2];
                this.data('toggleclicked', 0);
                this.on('click',function(event) {
                    var data = $(this).data();
                    var tc = data.toggleclicked;
                    $.proxy(funcs[tc], this)(event);
                    data.toggleclicked = (tc + 1) % 2;
                });
                return this;
            };
    

    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $('.online-status').fadeToggle(200);
        $('.chatcontainer').fadeToggle(500);
        $("#wrapper").toggleClass("toggled");
    });
    $('[data-submenu]').submenupicker();
    $('.dropdown-toggle').dropdown()

  

    var activesubmenu;
    $( '.cssmenu > ul > li.menuitem' ).mouseenter( function(){
        $(this).addClass('active');
    } )
    .mouseleave( function(){
        if(!$(this).hasClass('current')){
            $(this).removeClass('active');
            
        }
    } );

    $('.cssmenu ul li.menuitem').clickToggle(function(event) { 
        if($(this).hasClass('has-sub')){
            console.log('occupied1A');
            $(this).children('ul').css('visibility', 'visible');  
            $( this).children('ul').addClass('active');
            $( this).addClass('active');
            $( this).addClass('current');

            if(activesubmenu){
                /* If the element clicked has a parent chat container dont remove chat */
                if(!$(event.target).parents('.chatcontainer').length){
                    console.log('occupied2A');
                    activesubmenu.children('ul').css('visibility', 'hidden');  
                    activesubmenu.children('ul').removeClass('active');
                    activesubmenu.removeClass('active');
                    activesubmenu.removeClass('current');
                    activesubmenu = null;
                }
            }
         
            activesubmenu = $(this);
        }else{
            if(activesubmenu){
                console.log('occupied3A');
                activesubmenu.children('ul').css('visibility', 'hidden');  
                activesubmenu.children('ul').removeClass('active');
                activesubmenu.removeClass('active');
                activesubmenu.removeClass('current');
                activesubmenu = null;
            }
        }

    },
    function(event) {
        if($(this).hasClass('has-sub')){
            if(!$(this).hasClass('current')){
                console.log('aquiB');
                $(this).children('ul').css('visibility', 'visible');  
                $( this).children('ul').addClass('active');
                $( this).addClass('active');
                $( this).addClass('current');

                if(activesubmenu){
                    console.log('occupied2B');
                    activesubmenu.children('ul').css('visibility', 'hidden');  
                    activesubmenu.children('ul').removeClass('active');
                    activesubmenu.removeClass('active');
                    activesubmenu.removeClass('current');
                    activesubmenu = null;
                }

                activesubmenu = $(this); 


            }else{
                console.log('aqui2B');
                /* If the element clicked has a parent chat container dont remove chat */
                if(!$(event.target).parents('.chatcontainer').length){

                    $(this).children('ul').css('visibility', 'hidden');  
                    $( this).children('ul').removeClass('active');
                    $( this).removeClass('active');
                    $( this).removeClass('current');   
                    activesubmenu = null;
                }
                activesubmenu = $(this); 
            }
        }else{
            if(activesubmenu){
                console.log('occupied3b');
                activesubmenu.children('ul').css('visibility', 'hidden');  
                activesubmenu.children('ul').removeClass('active');
                activesubmenu.removeClass('active');
                activesubmenu.removeClass('current');
                activesubmenu = null;
            }
        }
        // if($(this).hasClass('has-sub')){
        //     if(!$(this).hasClass('active')){
        //         $(this).children('ul').css('visibility', 'visible');  
        //         $( this).children('ul').addClass('active');
        //         $( this).addClass('active');
        //         $( this).addClass('current');
               
        //         activesubmenu = $(this);  
        //     }
        // }
        
    });


    // activesubmenu.clickToggle(function() { 
    //     console.log('here now');
    //     $(this).removeClass('subactive');
    // },
    // function() {
    // });





});


function myFunction(id){
    printout('hello' + id);

}


    function startChat(receiver_user_id, event){
        /* Make sure startChat only gets called once */
        $(event.target).parent('li').data('id',null);
        //var receiver_user_id = $('#receiver_user_id').val();
        var params = {receiver_user_id: receiver_user_id};
        $.get('/message/getAll', params, function(data){
            console.log(data);
            if(data!=0){
                var user_id_temp;
                var different_user = true;
                var items = [];
                $.each( data, function( key, val ) {
                    var itemclass = 'right';
                    if (user_id_temp != val.receiver_user_id){
                        different_user = true;
                        user_id_temp = val.receiver_user_id;
                    }
                    else
                        different_user = false;

                    if (val.sender_user_id == receiver_user_id) {
                        itemclass = 'left';
                        if (different_user) {
                            items.push( '<span class="chat-img pull-left">');
                            
                            
                        };
                    }else{
                        if (different_user) {
                            items.push( '<span class="chat-img pull-right">');
                            
                        };
                    }
                    if (different_user) {
                        items.push( '</span>');
                    };
                    items.push( '<div class="chat-body clearfix">');
                    items.push( '<div class="header">');
                    if (val.sender_user_id == receiver_user_id){
                        if (different_user) {
                            items.push( '<strong class="primary-font">'+val.sender_username+'</strong>');
                        };
                        items.push( '<small class="pull-right text-muted">');
                        items.push( '<span class="glyphicon glyphicon-time"></span>' + val.sent + '</small>');
                    }
                    else{
                        items.push( '<small class="text-muted">');
                        items.push( '<span class="glyphicon glyphicon-time"></span>' + val.sent + '</small>');
                        if (different_user) {
                            items.push( '<strong class="pull-right primary-font">'+val.sender_username+'</strong>');
                        };
                    }
                    items.push( '</div>');
                    items.push( '<p>'+val.message+'</p>');
                    items.push( '</div>');
                    
                        $( "<li/>", {
                            "class": itemclass +" clearfix",
                            html: items.join( "" )
                        }).appendTo( "#"+receiver_user_id );
                        items = [];
                });
                 
            }  
        });
        /* Scroll to the bottom of chat */
        $('.chat-body').animate({
            scrollTop: $('.chat-body').get(0).scrollHeight
        }, 500);
        setInterval( function() { getChatText(receiver_user_id); }, 5000);
    }

    function getChatText(id) {
      //var receiver_user_id = $('#receiver_user_id').val();
      var receiver_user_id = id;
      console.log('getting chat text:' + id);
      $.ajax({
        type: "GET",
        //url: "/refresh.php?lastTimeID=" + lastTimeID
        data: {receiver_user_id: receiver_user_id},
        url: "/message/get"
      }).done( function( data )
      {
        printout(data);
        if(data!=0){
            var user_id_temp;
            var different_user = true;
            var items = [];
            $.each( data, function( key, val ) {
                var itemclass = 'right';
                if (user_id_temp != val.receiver_user_id){
                    different_user = true;
                    user_id_temp = val.receiver_user_id;
                }
                else
                    different_user = false;

                if (val.sender_user_id == receiver_user_id) {
                    itemclass = 'left';
                    if (different_user) {
                        items.push( '<span class="chat-img pull-left">');
                        
                        
                    };
                }else{
                    if (different_user) {
                        items.push( '<span class="chat-img pull-right">');
                        
                    };
                }
                if (different_user) {
                    items.push( '</span>');
                };
                items.push( '<div class="chat-body clearfix">');
                items.push( '<div class="header">');
                if (val.sender_user_id == receiver_user_id){
                    if (different_user) {
                        items.push( '<strong class="primary-font">'+val.sender_username+'</strong>');
                    };
                    items.push( '<small class="pull-right text-muted">');
                    items.push( '<span class="glyphicon glyphicon-time"></span>' + val.sent + '</small>');
                }
                else{
                    items.push( '<small class="text-muted">');
                    items.push( '<span class="glyphicon glyphicon-time"></span>' + val.sent + '</small>');
                    if (different_user) {
                        items.push( '<strong class="pull-right primary-font">'+val.sender_username+'</strong>');
                    };
                }
                items.push( '</div>');
                items.push( '<p>'+val.message+'</p>');
                items.push( '</div>');
                
                    $( "<li/>", {
                        "class": itemclass +" clearfix",
                        html: items.join( "" )
                    }).appendTo( "#"+receiver_user_id );
                    items = [];
            });
             
        }
        // var jsonData = JSON.parse(data);
        // var jsonLength = jsonData.results.length;
        // var html = "";
        // for (var i = 0; i < jsonLength; i++) {
        //   var result = jsonData.results[i];
        //   html += '<div style="color:#333">(' + result.sent + ') <b>Kathy</b>: ' + result.message + '</div>';
        //   lastTimeID = result.id;
        // }
        // $('#view_ajax').append(html);
      });
    }

    function sendChatText(id){
        var chatInput = $('#chat-input-'+id+"").val();
        $('#chat-input-'+id+"").val('');
        //var receiver_user_id = $('#receiver_user_id').val();
        var receiver_user_id = id;
        if(chatInput != ""){
            var params = {chatInput: chatInput, receiver_user_id: receiver_user_id};
            
            $.get('/message/send', params, function(data){
                console.log(data);  
            });
        }

            var items = [];

                items.push( '<span class="chat-img pull-right">');
                
            items.push( '</span>');
        items.push( '<div class="chat-body clearfix">');
        items.push( '<div class="header">');
        
            items.push( '<small class="text-muted">');
            items.push( '<span class="glyphicon glyphicon-time"></span>' + 'val.sent' + '</small>');
                items.push( '<strong class="pull-right primary-font">'+'val.sender_username'+'</strong>');
        items.push( '</div>');
        items.push( '<p>'+chatInput+'</p>');
        items.push( '</div>');
        
            $( "<li/>", {
                "class": "right clearfix",
                html: items.join( "" )
            }).appendTo( "#"+id );
    }

    function appendChat(text, username, sent){

    }