$(function(){

    
    $('#calendar').fullCalendar({
        header: {
        center: 'month,week' // buttons for switching between views
    },
    views: {
        week: {
            type: 'agenda',
            duration: { days: 7 },
            buttonText: 'week'
        }
    },
    eventRender: function(event, element) {
        element.qtip({
            content: event.description,
            style: {
                classes: 'myCustomClass qtip-blue qtip-shadow qtip-rounded'
            }
        });
    },
    aspectRatio: 2.2,   
    });

    /* After rendering the callendar, get all pending and todo tasks with an ajax request */
    $.get('/kanbantask/getTasksCalendar', function(data){
        $.each( data, function( key, val ) {
            $.each( val, function( key, value ) {
                newEvent                = new Object();
                newEvent.title          = value.name;
                newEvent.description    = value.description;
                newEvent.start          = new Date(value.start_date);
                newEvent.end            = new Date(value.due_date);
                $('#calendar').fullCalendar( 'renderEvent', newEvent, true);
            });
        });
    });
    


    $( "#selectable" ).selectable();
});